#!/usr/bin/perl -w
use strict;

#---------------------------------
# Convert sCNA changes   
# This script has been written to convert the new way of recording sCNA changes in 0.7 (1 = 0.5, 2=1, 3=1.5, etc), into a format that can be compared wothe array data
          
         
# Introduction of QC 
# The second version of this script performs 
# It generates a second summary file 
# In order to achieve this, it requires the cumr 
# Determine the number  
#--------------------------------

#example
#perl NEW_PRODUCTION.pl /panfs/home/arobertson/7_HitSeq/out/spellman_met_ori/analysis/spellman_met_ori.2.100.0528225013_1_all_1_0mb_X_300mb/cnv/ spellman_met_ori_i_23_i_[null]_i_[null]_e_all.txt

#define ARGV variables (to be explaned)
my $LOCATION_NAME=$ARGV[0];	#location of file
my $FILE_NAME=$ARGV[1];		#file name

my $WINDOW_SIZE=$ARGV[2];	#cumR
my $EXP_ID=$ARGV[3]; 		#name used in script
my $MODE=$ARGV[4]; 		#mode  
my $RANGE=$ARGV[5]; 		#range1
my $BDW=$ARGV[6];		#BDW
my $PLOIDY=$ARGV[7];		#BC
my $TUM_LOC=$ARGV[8];		#location of tumour bam
my $REF_LOC=$ARGV[9];		#location of normal bam
my $GC=$ARGV[10]; 

my $GenomeSeg=3137161; #Number of kilobase segments in the genome 3137161 / 2897310
my $PotSeg=0;

my $loc="$LOCATION_NAME";
my $name="$FILE_NAME";
#my $cumR="$WINDOW_SIZE";

#to cut
print "location = $loc \n";
print "file name = $name \n";

#define file names
my $new_name="_Edited_CNV.txt";
my $QC="SUMMARY.txt";

open (INPUT,"$loc/$name") or die "couldn't open the results file: $!\n";

open  OUT,">$loc/$name.$new_name";
open  OUT2,">$loc/$name.$QC";

#prepare summary file
print OUT2 "Experimental ID:\t\t$EXP_ID \n";
print OUT2 "Reference Bam:\t\t\t$REF_LOC \n";
print OUT2 "Tumour Bam:\t\t\t$TUM_LOC \n";
print OUT2 "MODE:\t\t\t\t$MODE \n";
print OUT2 "Location:\t\t\t$RANGE \n";
print OUT2 "Window Size:\t\t\t$WINDOW_SIZE Kb \n";
print OUT2 "Beta Down Weight:\t\t$BDW \n";
print OUT2 "Ploidy / BackgroundCount:\t$PLOIDY \n";
print OUT2 "GC Correction:\t\t\t$GC \n";

#define variables for analysis
my @data = ();          #array containing raw data
my @trim = ();
my $chr = '';
my $type = '';
my $CNV = '';

my $average = 0;
my $length = 0;		#new length in normal notation
my $counts = 0;		#number CNVs
my $running = 0;	#total length of CNVs
my $segs = 0;

while (<INPUT>) {
        my $line = $_;
        chomp $line;
        @data = split /\s+/, $line;

	# 1. print new header in edited output file
	if ($data[0] eq 'Sample')  
		{ print OUT "Sample\tChr\tFirstProbe\tLastProbe\tNbSnp\tlength_bp\tType\tAvg_certainty\n"; next;}
	
	# 2. print cellularity data in summary document 	
	if ($data[0] =~/##/) {
	@trim = split /\:/, $data[0];
	print OUT "$line\n"; print OUT2 "$line\n"; next;}

	# 3. replace chromosome X with chromosome 23
        $chr = $data[1];
         if ($chr eq 'X') {$chr = '23';}

	# 4. double CNV (0.74 relies on this)
	$CNV = $data[6];
        $type = $CNV + $CNV;

	# 5. QC step 
        $counts++; #increase the number of CNVs identified
        $length = $data[3]-$data[2];
        $running = $length + $running;
	
	# 6. print edited output
	print OUT "$data[0]\t$chr\t$data[2]\t$data[3]\t$data[4]\t$length\t$type\t$data[7]\n";
	
 }

# Step 2. Produce Summary Document 
	my $threshold = 1;
	
	$segs = $GenomeSeg/$WINDOW_SIZE;
	$threshold = $segs*0.0333;			#Threshold for too many CNVs is 3.333% of total 
	print OUT2 "Max. possible CNVs:\t\t$segs\n";

        $average = $running / $counts;

        print OUT2 "Number of CNVs:\t\t\t$counts\n";
#	print OUT2 "Threshold:\t\t\t$threshold CNVs \n";
	print OUT2 "Total length of CNVs:\t\t$running bp\n";
        print OUT2 "Average Length of CNV:\t\t$average bp\n";

	if ($counts > $threshold) {
	 print OUT2 "WARNING\tTHIS SAMPLE HAS FAILED QC: Too many CNVS \n";} 
	else {
	 print OUT2 "WARNING:\t\t\tNONE\n";}


