#!/usr/bin/perl -w

#----------
# This script has been written to add information about the allele frequency of SNPs identifed in the vsqr.vep.vcf  
# Original data from the Garvan
# Processed data by WARD_part_one and WARD_part_twob
# Additional information from EXAC
#----------

# Things to do
# 1. Split the results from WARD_PART_TWOb.pl
# 1a.	Ref Alt ALLELE FREQUENCY Wt.Codon

# 2. Collect AF from EXAC
# 2.a split EXAC
# if ALT = ALT -> split EXAC7[11]
 
#2.c if 

open EXAC, "/panfs/home/arobertson/DATA/ExAC.r0.3.1.sites.vep.vcf" or die "couldn't open the EXAC file: $!\n";

@shit_tonne=();
%MD_HASH=();
@data=();
$count=0;
%ALT_HASH=();
$BLOODY_HELL='';

#$grades{"Foo Bar"}{Mathematics}   = 97;

while (<EXAC>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
	if ($data[0] =~ /#/) {next;}
	$chr=$data[0];
	$pos=$data[1];
	$alt=$data[4];
	$idd="chr"."$chr".":"."$pos";

	@shit_tonne = split  /\;/, $data[7];
	#print "chr: $chr \t pos: \t $pos \t AF:\t$shit_tonne[11] \n";
	if ($shit_tonne[12] =~ /AF/) {$BLOODY_HELL = $shit_tonne[12];}
	if ($shit_tonne[11] =~ /AF/) {$BLOODY_HELL = $shit_tonne[11];}
	#if ($shit_tonne[10] =~ /AF/) {$BLOODY_HELL = $shit_tonne[10];}
	
	$MD_HASH{$chr}{$pos}="$BLOODY_HELL";
	$ALT_HASH{$chr}{$pos}="$alt";
	$count ++;
	#if ($count==1000000000){print "count \n"};
}

#print "EXAC done \n";



open DATA, "/panfs/home/arobertson/TRANSFER/Output_test.txt" or die "couldn't open the GENELIST file: $!\n";

@test=();
%PART_ONE_HASH=();
%PART_TWO_HASH=();
$ALT='';
$PRE_ID='';
$GATTAI='';
$CHR='';
$LOC='';
@bata=();

while (<DATA>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
	$ALT=$data[5];
	$PRE_ID=$data[2];
	@test= split  /:/, $data[2];
	$LOC=$test[1];
	@bata= split  /chr/, $test[0];  
	$CHR=$bata[1];
	@mest = ();

#	print "$PRE_ID \t chr: $CHR \t pos: \t $LOC \n";

	if (exists $MD_HASH{$CHR}{$LOC}) {
		if ($ALT_HASH{$CHR}{$LOC} eq $ALT ) {
		#if REF = ALT
		 print "$data[0]\t$data[1]\t$data[2]\t$data[3]\t$data[4]\t$data[5]\t$MD_HASH{$CHR}{$LOC}\t$data[6]\t$data[7]\t$data[8]\t$data[9]\t$data[10]\t$data[11]\t$data[12]\t$data[13]\t$data[14]\t$data[15]\t$data[16]\t$data[17]\t$data[18]\t$data[19]\t$data[20]\t$data[21]\t$data[22]\t$data[23]\t$data[24]\t$data[25]\n";
		 } else {	
		
		@best = split  /,/, $ALT_HASH{$CHR}{$LOC};
		@mest = split  /,/, $MD_HASH{$CHR}{$LOC};
		$array_count = 0;

		foreach $Z (@best) {
			if ($Z eq $ALT) { 
				print "$data[0]\t$data[1]\t$data[2]\t$data[3]\t$data[4]\t$data[5]\tAF=$mest[$array_count]\t$data[6]\t$data[7]\t$data[8]\t$data[9]\t$data[10]\t$data[11]\t$data[12]\t$data[13]\t$data[14]\t$data[15]\t$data[16]\t$data[17]\t$data[18]\t$data[19]\t$data[20]\t$data[21]\t$data[22]\t$data[23]\t$data[24]\t$data[25]\n";
				next;
			} else {$array_count++; }			
		}
	
	} 
	} else {print "$data[0]\t$data[1]\t$data[2]\t$data[3]\t$data[4]\t$data[5]\tNOVEL_SNP\t$data[6]\t$data[7]\t$data[8]\t$data[9]\t$data[10]\t$data[11]\t$data[12]\t$data[13]\t$data[14]\t$data[15]\t$data[16]\t$data[17]\t$data[18]\t$data[19]\t$data[20]\t$data[21]\t$data[22]\t$data[23]\t$data[24]\t$data[25] \n"; }
}
#foreach $X (sort keys %PART_ONE_HASH) {
#	print "$X\tP1:\t$PART_ONE_HASH{$X}\tP2:\t$PART_TWO_HASH{$X}\n";
#}
