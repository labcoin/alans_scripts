#!/usr/bin/perl -w

#This script has been written to take a gene list, and extract the location of these genes, and look in other samples, be they normalxnormal or other SameSamplexOtherNormal 

#-------------------
# Part One - Load results from overlap
#--------------------

# information is stored in the results  as:
# type	test	reference	chr	st(Mb)	end(Mb)	type	len(Mb)

open GENE_LIST, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/G348_065x1084GENE_LEVEL.txt" or die "couldn't open the blacklist file: $!\n";

#use MATH::Round;

my %GL_HASH = ();	#ENSG = $line
my %GL_CHR = ();	#ENSG = chrx	
my %GL_START = ();	#ENSG = 444545344
my %GL_STOP = ();	#ENSG = 444545344
my %OL_TYPE= ();

%CON1_HASH=();
%CON2_HASH=();
%VS098_HASH=();

while (<GENE_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
		if ($data[0] eq "Gene" ) {print "Starting analysis \n"; next;}
	$ID=$data[1];
	$GL_HASH{$ID}="$line";
	$temp_LOC=$data[4];
		@ddata = split  /:/, $temp_LOC;
		@cdata= split  /chr/, $ddata[0];
		$CHR = $cdata[1];
		@edata = split  /-/, $ddata[1];
		$START="$edata[0]";
		$STOP="$edata[1]";
		#print "$ID\t$CHR\t$START\t$STOP\n";

	$GL_CHR{$ID}="$CHR";
	$GL_STOP{$ID}="$STOP";
	$GL_START{$ID}="$START";

}



#--------------------
# Part two
#--------------------

open CONTROL1, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_065x1084_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the results file: $!\n";

while (<CONTROL1>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
	if ($data[0] =~ /#/) {next;}
	$CON_CHR="$data[1]";
		if ($CON_CHR eq 'Chr') {next;}
	$CON_START="$data[2]";
	$CON_STOP="$data[3]";

	#print "$CON_CHR \n";	


	foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON1_HASH{$ENS_ID}) {} else {$CON1_HASH{$ENS_ID}="NO"} 
		$GENE_CHR=$GL_CHR{$ENS_ID};
		$GENE_START=$GL_START{$ENS_ID};
		$GENE_STOP=$GL_STOP{$ENS_ID};
		if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; }

		 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; } 	
		 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
		 $CON1_HASH{$ENS_ID}="YES"; }

		if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
		$CON1_HASH{$ENS_ID}="YES"; } 

		}#Close for each
}

#--------------------------------------------------------------------

#-----------------------------------------------------------------------
open CONTROL2, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_1084x065_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the results file: $!\n";

while (<CONTROL2>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON2_HASH{$ENS_ID}) {} else {$CON2_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON2_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON2_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

open V098, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_G348v098_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the results file: $!\n";

while (<V098>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";
#VS098_HASH
        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                 if (exists $VS098_HASH{$ENS_ID}) {} else {$VS098_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $VS098_HASH{$ENS_ID}="YES"; } 

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $VS098_HASH{$ENS_ID}="YES"; } 

                }#Close for each
}

#---------------------------------------------------

open  OUT_FINAL, ">/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/G348_FINAL_OVERLAP.txt";

print OUT_FINAL "Gene\tEnsembl-ID\tGene-Type\tStatus\tLocation\tCNV-type\tNo.CNVs\tCNVs\tMMR-GENE\t065v1084\t1084v065\t348v098\n";

 foreach my $ENS_ID (sort keys %GL_HASH) {
	print OUT_FINAL "$GL_HASH{$ENS_ID}\t$CON1_HASH{$ENS_ID}\t$CON2_HASH{$ENS_ID}\t$VS098_HASH{$ENS_ID}\n";
} 

=cut

# END END END END END




open GENE_LIST, "/panfs/home/arobertson/DATA/ENSEMBL_GENELIST.txt" or die "couldn't open the results file: $!\n";

	%COUNTS = ();
	%GENES_PER_CNV = ();
	%SYM_PER_CNV = ();

	%GENE_HASH = ();
	%GENE_DATA = ();
	%GENE_C2 = ();
	%TYPE_HASH2 = ();

while (<GENE_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;

	#print "$data[0]  \t $data[1] \n";
	$GENE_ID=$data[0];
	if ($data[0] eq "Ensembl Gene ID" ) {next;}
	$GENE_SYM=$data[1];
	if ($data[2] =~ /^[0-9,.E]+$/) {} else {next;}
	$GENE_CHR=$data[2];
	$GENE_START=$data[3];
	$GENE_STOP=$data[4];
	$GENE_LOC="chr$GENE_CHR:$GENE_START-$GENE_STOP";
	$GENE_DATA{$GENE_SYM}="$data[6]\t$data[8]\t$GENE_ID\t$GENE_LOC";
	$GENE_COUNT=0;
	$GENE_COUNT2=0;


        foreach my $CNV_ID (sort keys %OL_HASH) { #for each CNV location
                $CNV_CHR         =       $OL_CHR{$CNV_ID};
                $CNV_START       =       $OL_START{$CNV_ID};
                $CNV_STOP        =       $OL_STOP{$CNV_ID};
		$CNV_TYPE	 =	 $OL_TYPE{$CNV_ID};
		#$SYM_PER_CNV{$CNV_ID} = "N/A";

		if ($CNV_CHR == $GENE_CHR ) {} else {next;}

		 #MODE A - if gene is inside a CNV
                 if ($CNV_START < $GENE_START && $CNV_STOP > $GENE_STOP) {
                	if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}	
			 $GENE_COUNT++;
			 $COUNTS{$CNV_ID}=$GENE_COUNT;
			if (exists $SYM_PER_CNV{$CNV_ID}) {  
			$SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(I)"}
			else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(I)"}
			
			if (exists $GENE_HASH{$GENE_SYM}) {	
			$GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(I)"}
			else {
			$GENE_HASH{$GENE_SYM}="$CNV_ID.(I)"; 
			$TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }	;	 
		
			if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};} 
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;

		#print "$GENE_SYM\t is inside $CNV_ID \n";
                 #;
		} #close MODE A
		
		#Start MODE B
		if ($GENE_START > $CNV_START && $GENE_START < $CNV_STOP && $GENE_STOP > $CNV_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(P3)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(P3)"} ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(P3)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(P3)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;

		} #close MODEB	

                #Start MODE C
                if ($GENE_START < $CNV_START && $GENE_STOP > $CNV_START &&  $GENE_STOP < $CNV_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(5P)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(5P)"} ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(5P)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(5P)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;
                } #close MODEC  

                 #MODE D - if gene is spans CNV
                 if ($CNV_START > $GENE_START && $CNV_STOP < $GENE_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(S)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(S)"}  ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(S)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(S)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;
                #print "$GENE_SYM\t is inside $CNV_ID \n";
                 #;
                } #close MODE A


		} #close CNV_ID look	

}

%MMR_LIST=();
$MMR_LIST{MSH6}=1;
$MMR_LIST{MSH5}=1;
$MMR_LIST{MSH4}=1;
$MMR_LIST{MSH3}=1;
$MMR_LIST{MSH2}=1;
$MMR_LIST{MLH1}=1;

$MMR_LIST{PCNA}=1;
$MMR_LIST{LIG1}=1;
$MMR_LIST{RPA1}=1;
$MMR_LIST{RPA3}=1;
$MMR_LIST{POLD1}=1;
$MMR_LIST{POLD2}=1;
$MMR_LIST{POLD3}=1;
$MMR_LIST{POLD4}=1;
$MMR_LIST{EXO1}=1;


open  OUT, ">/panfs/home/arobertson/DATA/WARD_DATA/G252_OVERLAP.txt";
print OUT "CNV\tTYPE\tNo.Genes\tGenes\n";
		
	foreach my $ID (sort keys %COUNTS) { 
		print OUT "$ID\t$OL_TYPE{$ID}\t$COUNTS{$ID}\t$SYM_PER_CNV{$ID}\n";
		}

open  OUT1, ">/panfs/home/arobertson/DATA/WARD_DATA/G252_OVERLAP_GENES.txt";
		
		print OUT1"Gene\tGene-Type\tStatus\tEnsemblID\tLocation\tCNV-type\tNo.CNVs\tCNVs\tMMR-GENE\n";

        foreach my $GENES (sort keys %GENE_HASH) {
                if (exists $MMR_LIST{$GENES}) {$MMR="YES"} else {$MMR="NO"}
		print OUT1 "$GENES\t$GENE_DATA{$GENES}\t$TYPE_HASH2{$GENES}\t$GENE_C2{$GENES}\t$GENE_HASH{$GENES}\t$MMR\n";
                }

print "CNVs annotated \n";
