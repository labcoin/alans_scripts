#!/usr/bin/perl -w

#This script has been written to take a gene list, and extract the location of these genes, and look in other samples, be they normalxnormal or other SameSamplexOtherNormal 

#-------------------
# Part One - Load results from overlap
#--------------------

# information is stored in the results  as:
# type	test	reference	chr	st(Mb)	end(Mb)	type	len(Mb)

$SAMPLE_NAME=$ARGV[0];
$name="$SAMPLE_NAME";
$file_name="$name"."GENE_LEVEL.txt";
$final_name="$name"."FINAL.txt";
#print "$name \n";
@temp_data=split /065/, $name;
$ward_name=$temp_data[0];
chop $ward_name;
#print "$ward_name \n";
$alt_con="NEW_$ward_name"."v098_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt";

open GENE_LIST, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$file_name" or die "couldn't open the GENELIST file: $!\n";



#use MATH::Round;

my %GL_HASH = ();	#ENSG = $line
my %GL_CHR = ();	#ENSG = chrx	
my %GL_START = ();	#ENSG = 444545344
my %GL_STOP = ();	#ENSG = 444545344
my %OL_TYPE= ();

%CON1_HASH=();
%CON2_HASH=();
%VS098_HASH=();

while (<GENE_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
		if ($data[0] eq "Gene" ) {print "Starting analysis \n"; next;}
	$ID=$data[1];
	$GL_HASH{$ID}="$line";
	$temp_LOC=$data[4];
		@ddata = split  /:/, $temp_LOC;
		@cdata= split  /chr/, $ddata[0];
		$CHR = $cdata[1];
		@edata = split  /-/, $ddata[1];
		$START="$edata[0]";
		$STOP="$edata[1]";
		#print "$ID\t$CHR\t$START\t$STOP\n";

	$GL_CHR{$ID}="$CHR";
	$GL_STOP{$ID}="$STOP";
	$GL_START{$ID}="$START";

}



#--------------------
# Part two
#--------------------

open CONTROL1, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_065x1084_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control1 file: $!\n";

while (<CONTROL1>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
	if ($data[0] =~ /#/) {next;}
	$CON_CHR="$data[1]";
		if ($CON_CHR eq 'Chr') {next;}
	$CON_START="$data[2]";
	$CON_STOP="$data[3]";

	#print "$CON_CHR \n";	


	foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON1_HASH{$ENS_ID}) {} else {$CON1_HASH{$ENS_ID}="NO"} 
		$GENE_CHR=$GL_CHR{$ENS_ID};
		$GENE_START=$GL_START{$ENS_ID};
		$GENE_STOP=$GL_STOP{$ENS_ID};
		if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; }

		 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; } 	
		 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
		 $CON1_HASH{$ENS_ID}="YES"; }

		if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
		$CON1_HASH{$ENS_ID}="YES"; } 

		}#Close for each
}

#--------------------------------------------------------------------

#-----------------------------------------------------------------------
open CONTROL2, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_1084x065_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL2>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON2_HASH{$ENS_ID}) {} else {$CON2_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON2_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON2_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

open V098, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$alt_con" or die "couldn't open the $ward_name vs 098 file: $!\n";

while (<V098>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";
#VS098_HASH
        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                 if (exists $VS098_HASH{$ENS_ID}) {} else {$VS098_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $VS098_HASH{$ENS_ID}="YES"; } 

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $VS098_HASH{$ENS_ID}="YES"; } 

                }#Close for each
}

#---------------------------------------------------

open  OUT_FINAL, ">/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$final_name";

print OUT_FINAL "Gene\tEnsembl-ID\tGene-Type\tStatus\tLocation\tCNV-type\tNo.CNVs\tCNVs\tMMR-GENE\t065v1084\t1084v065\t348v098\n";

 foreach my $ENS_ID (sort keys %GL_HASH) {
	print OUT_FINAL "$GL_HASH{$ENS_ID}\t$CON1_HASH{$ENS_ID}\t$CON2_HASH{$ENS_ID}\t$VS098_HASH{$ENS_ID}\n";
} 

=cut

# END END END END END            




