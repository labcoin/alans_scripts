#!/usr/bin/perl -w

#----------
# This script has been written to compare the SNVs identified in 1249,1494 and 1524 to those identifed in the 609 and 616 

#----------

#logic
#make id for each SNP = chrXX_LOCATION_wt_SNP
#I know I don't need the wild type allele, but I'm using it as a check
#for each file store ID in combined has and individual hash

# Part One - Variables 

%COMBINED_HASH=();
%HASH_616=();
%HASH_609=();

@data=();
$ID=();

# Part two
##NORMALS

open FILE_609, "/panfs/home/arobertson/TRANSFER/Plasma_VCFs/065_P0.filtered.vcf.snp.txt" or die "couldn't open the 609 file: $!\n";

while (<FILE_609>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
        $COMBINED_HASH{$ID}="1";
        $HASH_609{$ID}="$data[10]";
        }

close FILE_609;
print "609 read \n";

open FILE_616, "/panfs/home/arobertson/TRANSFER/Plasma_VCFs/098_P0.filtered.vcf.snp.txt" or die "couldn't open the 616 file: $!\n";

while (<FILE_616>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
        $COMBINED_HASH{$ID}="1";
        $HASH_616{$ID}="$data[10]";
        }

close FILE_616;
print "616 read \n";


###### FILL IN EMPTY VALUES

open  OUT, ">/panfs/home/arobertson/TRANSFER/Plasma_VCFs/allplasmaNORMALS.txt";
print OUT "SNV_ID\t609P0\t616P0\n";

foreach $X (sort keys %COMBINED_HASH) {
	if ((exists $HASH_616{$X}) && (exists $HASH_609{$X})) {
		print OUT "$X\t$HASH_609{$X}\t$HASH_616{$X}\n";
}
}

##### PRINT OUT

