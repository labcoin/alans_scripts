#!/usr/bin/perl -w

use strict;

#-----------------------------------
#qXSV
#Expressed SV Finder
#----------------------------------

# 1. open recurrent sCNA matrix
# 2. read into array
	#:wq!
#chr
#
#


#------------------------------------
my $NAME = 'RECURRENT';

my @data = ();  
my $R_CHR = '';  
my $R_START = ''; 
my $R_STOP = '';   
my $R_CHANGE = '';  
my $R_SOURCE = '';
my $Rx = '';
my $Ry = '';
my %R_HASH = ();
#my @R_HASH = ();

open RCNAs, "/panfs/home/arobertson/SCRIPTS/scripts/RecurrentsCNAs.csv" or die "couldn't open the results file: $!\n";

while (<RCNAs>) {
	my $line = $_;
        chomp $line;
	print "$line \n"
	die;
        @data = split /','/, $line;
        $R_CHR = $data[0];
		if ($R_CHR eq "Chromosome") {next;}
	$R_START = $data[1];
	$R_STOP = $data[2];
	$R_CHANGE = $data[3];
	$R_SOURCE = $data[4];
	
	print "$R_START \n";
	print "$R_STOP \n";
	print "$R_CHANGE \n";
	print "$R_SOURCE \n";

	
	$Rx = "$R_CHR"."\t"."$R_START"."\t"."$R_STOP"."\t"."$R_CHANGE";
        $Ry = "$R_CHR"."\t"."$R_START"."\t"."$R_STOP";
	$R_HASH{$Ry}=$Rx;
}
	
die;
foreach my $X (sort keys %R_HASH) {  
	print "$R_HASH{$X} \n";
}
