#!/usr/bin/perl -w

use strict;

#-----------------------------------
#qXSV
#Expressed SV Finder
#----------------------------------

#This script was written by ARobertson on 7/12/13-22/1/14. It is based on work completed across 2011 and 2012.

#This script examines the results from qSV, identifies candidate SVs then examines RNA-Seq bams to see if is any evidence to support these rearrangements.

#---------------------------------
# Part Minus 1
# Reading in the variables defined in the bash script
# APGI, TYPE, BAM LOCATION

my $usage = "usage perl $0 <apgi> \n";

print join("\t", @ARGV),"\n";

my $APGI=$ARGV[0];
my $TYPE=$ARGV[1];
my $BLAM=$ARGV[2];

my $MAPQ = '';				#Will be used to exclude reads of low quality - set by user
my $COMBINED = "$APGI"."_X_"."$TYPE";

open OUT,  ">/panfs/home/arobertson/XSV/DATA/RSEM/$COMBINED-MODIFIED-SAM.txt";
open OUT3, ">/panfs/home/arobertson/XSV/DATA/RSEM/$COMBINED-SUMMARY.txt";
	

print OUT3 "PATERNAL-SAMPLE\tINTERNAL-SVID\tCLASS\tSYMBOL-FROM\tSYMBOL-TO\tPOSSIBLE-FUSION\tTOTAL-NUMBER-OF-READS\tTOTAL-NUMBER-OF-UNIQUE-READS\tTISSUE-TESTED\tENSGFROM\tCHR-FROM\tSTRAND-FROM\tFROM-START\tFROM-STOP\tSYMBOL-FROM-CHECK\tBREAKPOINT-FROM\tENSG-TO\tCHR-TO\tSTRAND-TO\tSTART-TO\tSTOP-TO\tSYMBOL-TO-CHECK\tBREAKPOINT-TO \n";

print OUT2 "PATERNAL-SAMPLE\tINTERNAL-SVID\tMAPQ\tSAMPLE-ID\tENSGFROM\tGENE-FROM\tEND1-START\tBREAKPOINT1\tDISTANCE-FROM-BP\tENSGTO\tGENE-TO\tEND2-START\tBREAKPOINT2\tDISTANCE-FROM-BP\n";

open BAM, "samtools view $BLAM |";
#---------------------------------

#----------------------------------
#Part #0
#Open the Ensembl Gene Matrix
#For each ensembl gene ID, store the location of the gene and it's symbol in a hash (i.e. chrA minus 1234 2345 KREX) these are all seperated by a tab :)
#-----------------------------------

my @mata = ();
my $echr = '';
my $eloc1 = '';
my $eloc2 = '';
my $estrand = '';
my $esym = '';
my $eID = '';
my $gattai = '';
my %GENEMATRIX = ();
my %SYMB = ();

open (ENS,"/panfs/home/arobertson/XSV/ENSEMBL70.txt ") or die "couldn't open the Ensembl Gene Matrix: $!\n";

while (<ENS>) {
        my $line = $_;
        chomp $line;
        @mata = split /\t+/, $line;
	$eID = $mata[0];
	if ($eID eq 'Ensembl Gene ID') {next;} 
	$esym = $mata[1];
	$echr = $mata[2];
	$eloc1 = $mata[4];
	$eloc2 = $mata[5];
	$estrand = $mata[3];
	$gattai = "$echr"."\t"."$estrand"."\t"."$eloc1"."\t"."$eloc2"."\t"."$esym";
	$GENEMATRIX{$eID}= $gattai;
	$SYMB{$eID} = $esym;
	#if ($eID eq 'ENSG00000108100') {print "qSV results exist in Ensembl gene list\n";}
	}
close ENS;

print "Gene Matrix Stored \n";

#----------------------------------
#Part #2
#qSV digestion
#read in qSV results, extract relevant records, save information to hash
#---------------------------------


my @qata = ();		#an array used to begin to process the qSV file
my %GRANDID = ();       # an ID that will be used to store all information surrounding each SV
my $count = 0;

my %BREAKPOINT = ();
my %ATLAS = ();		# a hash containing all the information about the tissue and the type i.e. APGI_1234_7Celline  
my %GENES = ();		# a hash containing the names of the genes this fusion is between
my %SUBTYPE = ();	# a hash descrbing the subtype of the translation	
my %SKIPPED = ();	# a hash containing the skipped SVS

my $CHRFROM = '';
my $CHRTO = '';
my $ENSGTO = '';
my $ENSGFROM = '';
my $MINIMUM = '';
my %SVFROM = ();	# A hash containing the location of the gene(s) that the SV occurs in	
my %SVTO = ();
my %SVFULL = ();	# a has containing all the information for each SV
my $pf = '';

my %INTER = (); 	# A hash containing the location of the interchromosomal rearrangements

my %GRAND_HASH = ();	# a super important multi-dimensional hash - $GRAND_HASH [chr1] [SV-ID] : location

my %POSSIBLE = ();

open (qSV,"/panfs/home/arobertson/XSV/APGI_100_WGS_candidate_genefusions_SUPER_FINAL.txt") or die "couldn't open the results from qSV: $!\n";

print "Reading qSV results \n";

my $SVID = '';
my @fix = ();
my @locator = ();
my $bp = '';

while (<qSV>) {
        my $line = $_;
        chomp $line;
        @qata = split /\t/, $line;

	if ($qata[0] eq 'analysis_id') {next;}		#skip header

	$count++;					#first thing is first, let's establish a unique ID for this SV
	$GRANDID{$count} = $line;
	
	my $SAMPLE_TYPE	= "$qata[42]"."_X_"."$qata[45]";		#APGI_1234_X_7primaryTumour
	$ATLAS{$count} = $SAMPLE_TYPE;
	$BREAKPOINT{$count} = "$qata[8]"."_X_"."$qata[13]";	
	
	$pf = $qata[67];
	$POSSIBLE{$count} = $pf;  
	
	$CHRFROM	 = $qata[7];
	$CHRTO		 = $qata[12];
	$ENSGTO		 = $qata[64];
	$ENSGFROM	 = $qata[60];
	$SVID		 = $qata[0];

	if ($CHRFROM eq $CHRTO) {$SUBTYPE{$count} = "INTRA";} else {$SUBTYPE{$count} = "INTER-CHR" };

		my $TEMPSMALL	 = '100000000000000000000';
		my $TEMPLARGE	 = '0';
		
		if ($ENSGTO eq $ENSGFROM ) {$SKIPPED{$count} = "SAME GENE"; next;}	#Sorry, this isn't the script you're looking for. 
		
		if ($ENSGTO =~ /\|/) {my @array = split /\|/, $ENSGTO;
			foreach my $VV (@array){
				if ($ENSGFROM =~ $VV) {$SKIPPED{$count} = "INCLUDES SAME GENE"; next; next;	}
					}
				}

                if ($ENSGFROM =~ /\|/) {my @array = split /\|/, $ENSGFROM;
                        foreach my $VV (@array){
                                if ($ENSGTO =~ $VV) {$SKIPPED{$count} = "INCLUDES SAME GENE"; next; next;   }
                                        }
                                }

		$GENES{$count} = "$ENSGFROM"."\t"."$ENSGTO";

#because things aren't straigh forward and sometimes genes overlap, I've written this bit of code to determine from the beginning of the first gene to the end of the last gene

		if ($ENSGFROM =~ /\|/) {
			@fix = split /\|/, $ENSGFROM;
			my %ENSGHASH = ();
			
			foreach my $Z  (@fix) {					#for each of the geneIDS that the SV falls within
				my $Y = $GENEMATRIX{$Z};
				@locator = split /\t/, $Y;
				my $Xsmall = $locator[2];
				my $Xbig = $locator[3];
					if ($Xsmall < $TEMPSMALL) {$TEMPSMALL = $Xsmall;}  
                                        if ($TEMPLARGE - $Xbig <0) { $TEMPLARGE = $Xbig;} else {next};			
					}	
				$SVFROM{$count} =  "$locator[0]\t$locator[1]\t$TEMPSMALL\t$TEMPLARGE\tspans multiple genes";}
	 		else {
				my $Y = $GENEMATRIX{$ENSGFROM};		# y = chr stand start stop symbol
				$SVFROM{$count} = $Y;
			}	

                if ($ENSGTO =~ /\|/) {
                        @fix = split /\|/, $ENSGTO;
                        my %ENSGHASH = ();
				foreach my $Z  (@fix) {				
					my $Y = $GENEMATRIX{$Z};
					@locator = split /\t/, $Y;
					my $Xsmall = $locator[2];
					my $Xbig = $locator[3];
						if ($Xsmall < $TEMPSMALL) {$TEMPSMALL = $Xsmall;}
						if ($TEMPLARGE - $Xbig <0) { $TEMPLARGE = $Xbig;} else {next};
						}
				$SVTO{$count} =  "$locator[0]\t$locator[1]\t$TEMPSMALL\t$TEMPLARGE\t spans multiple genes";
			} else { my $Y = $GENEMATRIX{$ENSGTO}; $SVTO{$count} = $Y; }
	 }

close qSV;

#process location hashes to produce standard id for searching bam

my @tata = (); 		#to array
my @fata = (); 		#from array
my $chra = '';		#chromosome from
my $chrb = '';		#chromsome to
my $as = '';		#from-start
my $ae = '';		#from-end
my $bs = '';		#to-start
my $be = '';		#to-end


foreach my $X (sort keys %GRANDID) {		#foreach SV (sv = $X)
	if ($SVTO{$X} eq '') {next;}
	if ($SVFROM{$X} eq '') {next;}	

	my $A = $SVFROM{$X};
	@fata = split /\t/, $A;
	$chra = "chr"."$fata[0]";
	$as = $fata[2];
	$ae = $fata[3];	

	my $B = $SVTO{$X};
	@tata = split /\t/, $B;
	$chrb = "chr"."$tata[0]";
	$bs = $tata[2];
	$be = $tata[3];

		if ($chra eq $chrb) { $GRAND_HASH{$chra}{$X} = "$chra:$as:$ae:$chrb:$bs:$be"}
			 else {$INTER{$X} = "$chra:$as:$ae:$chrb:$bs:$be"};	
	}

#$GRANDHASH{chr1}{103} = chr1:-1:1234:1234:chr1:1:2345:23345
#$CHROMSOME[chr1][svID]

print "processing of qSV complete \n";

#ALRIGHT, LET'S DO THIS!
#LET'S BRING IN THE BAM!

#---------------------------------
# Part 2
# Identifcation of the specific reads that support the SVs
# NOTE: before running this part you need to load samtools onto the node before it will work 
#--------------------------------

my @SAM = ();
my @BOXER = ();	#a work horse array. 	
my %SUPPORT = (); #a hash containing the reads that support the SV

while(<BAM>){
        next if(/^(\@)/);                             #don't process head 
        my $line = $_;
        chomp $line;
	my @SAM = split /\t+/, $line;

#Define the varibles
        my $ID = $SAM[0];
        my $CHRA = $SAM[2];
        my $CHRB = $SAM[6];
                if ($CHRB eq '*') {next;}               #remove singletons
	my $LOCA = $SAM[3];
        my $LOCB = $SAM[7];
        my $MAPQUAL = $SAM[4];

#Process the read
	if ($CHRB eq '=') { 		#if the pairs are on the same chromosome
		foreach my $X (sort keys %{$GRAND_HASH{$CHRA}}) {		#for each of the IDs that are on this chromosome
			my $Y = $GRAND_HASH{$CHRA}{$X};				#get the location of each gene containing the bp
			@BOXER = split /:/, $Y;
                        my $LOCFROMSTART = $BOXER[1];
                        my $LOCFROMSTOP = $BOXER[2];
                        my $LOCTOSTART = $BOXER[4];
                        my $LOCTOSTOP = $BOXER[5];
                                        
			if ($LOCA > $LOCTOSTART && $LOCA < $LOCTOSTOP) {
                        	if ($LOCB > $LOCFROMSTART && $LOCB < $LOCFROMSTOP) {    
                                	if (exists $SUPPORT{$X}) {my $W = $SUPPORT{$X}; $SUPPORT{$X} = "$W\n$line";
                                               }  else {$SUPPORT{$X} = $line;}
					} else {next;}
			} else {next;}
		}
	} else {		#if the pairs belong to different chromosomes
		foreach my $X (sort keys %INTER) {
        		my $Y = $INTER{$X};
               		 @BOXER = split /:/, $Y;
        	        my $CHRFROM = $BOXER[0];
               		my $CHRTO = $BOXER[3];
                	my $LOCFROMSTART = $BOXER[1];
                	my $LOCFROMSTOP = $BOXER[2];
                	my $LOCTOSTART = $BOXER[4];
                	my $LOCTOSTOP = $BOXER[5];

                	if ($CHRA eq $CHRTO && $CHRB eq $CHRFROM) {
                		if ($LOCA > $LOCTOSTART && $LOCA < $LOCTOSTOP) {
                        		if ($LOCB > $LOCFROMSTART && $LOCB < $LOCFROMSTOP) {
                                		if (exists $SUPPORT{$X}) {my $W = $SUPPORT{$X}; $SUPPORT{$X} = "$W\n$line";
                                	        } else {$SUPPORT{$X} = $line;}
                                                } else {next:}
                                        } else {next;}
                                }  else {next;}
			}
	}
}

close BAM;

my @amata = (); 
my @BPATA = ();
my %SUMMER = ();
my %COUNTER =  ();
my @READ = ();
my %SPANNERFROM = ();
my %SPANNERTO = ();
my $symbol = '';
my %UNIQUE = ();

foreach my $Z (sort keys %SUPPORT) {			#foreach of the global ids ($Z), which have supporting reads
		if (exists $SKIPPED{$Z}) {next;}	#skip the SVs which aren't quite right

		my $count1 = 0;		my %UNI = ();

#---------------------------------------------------
# Individual reads processing beings 
#-------------------------------------------------
		@READ = split /\n/, $SUPPORT{$Z};
		foreach my $record (@READ) {			#indivudal reads

			@amata = split /\t/, $record;	
			$count1 ++;
			my $L1 = $amata[3];
			my $L2 = $amata[7];		
			my $CH1 = $amata[2];
			my $CH2 = $amata[6];
			my $QUAIL = $amata[4];							

			if ($CH2 eq '=') {$CH2 = $CH1; }
                        my $v1 = "$CH1"."$L1"."$CH2"."$L2";
                        $UNI{$v1} = $Z;

					
			@BPATA = split /_X_/, $BREAKPOINT{$Z};
			my $bp1 = $BPATA[0];
			my $bp2 = $BPATA[1];

			my $dis1 = $L1 - $bp1;
			my $dis2 = $L2 - $bp2;
			$COUNTER{$Z} = $count1;

                        my $S = $GENES{$Z};                     #Get the ENSG IDs for the genes
                        my @cymbol = split /\t/, $S;
                        my $RFROM = $cymbol[0];
                        my $RTO = $cymbol[1];

                        if ($RFROM =~ /\|/) {
                                my @complex = split /\|/, $RFROM;               #foreach of the ensg IDs
                                foreach my $ENSGENSG (@complex) {
                                my $temp = $SYMB{$ENSGENSG};                    #get the symbol
                                        if (exists $SPANNERFROM{$Z}) {
                                        my $XXX = $SPANNERFROM{$Z};
                                        $SPANNERFROM{$Z} = "$XXX"."|"."$temp";
                                         } else {$SPANNERFROM{$Z} = $temp;}
                                                }
                                        }
                         else {$SPANNERFROM{$Z} = $SYMB{$RFROM};}

                        if ($RTO =~ /\|/) {
                                my @complex = split /\|/, $RTO;               #foreach of the ensg IDs
                                foreach my $ENSGENSG (@complex) {
                                my $temp = $SYMB{$ENSGENSG};                    #get the symbol
                                        if (exists $SPANNERTO{$Z}) {
                                        my $YYY = $SPANNERTO{$Z};
                                        $SPANNERTO{$Z} = "$YYY"."|"."$temp";
                                        } else {$SPANNERTO{$Z} = $temp;}
                                                }
                                        }
                         else {$SPANNERTO{$Z} = $SYMB{$RTO};}

			print OUT2 "$ATLAS{$Z}\t$Z\t$QUAIL\t$COMBINED\t$RFROM\t$SPANNERFROM{$Z}\t$CH1:$L1\t$bp1\t$dis1\t$RTO\t$SPANNERTO{$Z}\t$CH2:$L2\t$bp2\t$dis2\n";
		
	}

#-----------------------------------------------
# Processing for summary file
#----------------------------------------------
			$UNIQUE{$Z} = keys %UNI;
		
			my $S = $GENES{$Z};			#Get the ENSG IDs for the genes
			my @cymbol = split /\t/, $S;
			my $RFROM = $cymbol[0];
			my $RTO = $cymbol[1];

			if ($RFROM =~ /\|/) {
				my @complex = split /\|/, $RFROM;		#foreach of the ensg IDs
				foreach my $ENSGENSG (@complex) {
				my $temp = $SYMB{$ENSGENSG};			#get the symbol
					if (exists $SPANNERFROM{$Z}) {
					my $XXX = $SPANNERFROM{$Z};
					$SPANNERFROM{$Z} = "$XXX"."|"."$temp"; 
					 } else {$SPANNERFROM{$Z} = $temp;}
						}
					} 
			 else {$SPANNERFROM{$Z} = $SYMB{$RFROM};} 
			
                        if ($RTO =~ /\|/) {
                                my @complex = split /\|/, $RTO;               #foreach of the ensg IDs
                                foreach my $ENSGENSG (@complex) {
                                my $temp = $SYMB{$ENSGENSG};                    #get the symbol
                                        if (exists $SPANNERTO{$Z}) {
                                        my $YYY = $SPANNERTO{$Z};
                                        $SPANNERTO{$Z} = "$YYY"."|"."$temp";
                                        } else {$SPANNERTO{$Z} = $temp;}
                                                }
                                        }
                         else {$SPANNERTO{$Z} = $SYMB{$RTO};}			
 
#			print OUT3 "$COMBINED\t$Z\t$SPANNERFROM{$Z}\t$SPANNERTO{$Z}\t$COUNTER{$Z}\t$UNIQUE{$Z}\t$ATLAS{$Z}\t$GENES{$Z}\n";

			my $WATCH = $GENES{$Z};
			my @SSS = split /\t/, $WATCH;
			my $jeanfrom = $SSS[0];
			my $jeanto = $SSS[1];

			my @BPATAR = split /_X_/, $BREAKPOINT{$Z};
                        my $bp11 = $BPATAR[0];
                        my $bp22 = $BPATAR[1];

			print OUT3 "$ATLAS{$Z}\t$Z\t$SUBTYPE{$Z}\t$SPANNERFROM{$Z}\t$SPANNERTO{$Z}\t$POSSIBLE{$Z}\t$COUNTER{$Z}\t$UNIQUE{$Z}\t$COMBINED\t$jeanfrom\t$SVFROM{$Z}\t$bp11\t$jeanto\t$SVTO{$Z}\t$bp22\n";
	
#-------------------------------------
#Print all of the records that support each SV
#------------------------------------
	print OUT "$Z\n$ATLAS{$Z}\t$count1\n$SUPPORT{$Z} \n\n";

	}	

