#LIB=/panfs/home/arobertson/PlasmaCNV_0.3/lib/
java=/panfs/home/arobertson/CNV/jre1.6.0_19/bin/java
#pwd=$HOME/Distribution/PlasmaCNV_0.1/
pwd=/panfs/home/arobertson/PlasmaCNV_0.3/

#--------------------------
# USAGE - sh -v processQbam.sh <counts file> <all>
#--------------------------

#chrom="chr1:chr2"
chrom=$2
buffer=1

export LIB=$pwd/lib/
indir=$(echo $1  | cut -f 1 -d '/')
in_nme=$(echo $1  | cut -f 2 -d '/' | cut -f 1 -d '.')
type=$(echo $1  | cut -f 2 -d '/' | cut -f 2 -d '.')


cd $indir

infile=$in_nme"."$type
dir=$in_nme
echo $infile

mem=3900m


dat=$(date +%s)
ls $LIB>lib_list_$dat
while read line; do
class=$class:"$LIB/$line"
done < lib_list_$dat
rm lib_list_$dat


line="java -Xmx$mem -server -cp $class lc1.dp.appl.ConvertVCFToZip $dir build37 #Chrom $buffer $chrom"
echo  $line

$line

mv $dir/all.zip .
rm -rf all/
cd $pwd
