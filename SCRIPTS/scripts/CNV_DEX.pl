#!/usr/bin/perl -w

#This script has been written to remove the existing CNVs known to occur in 1524, 1249 and 1494 from the CNVs identified in the WARD bams

#it requires the Blacklist file as well as the output from sCNAHitSeq 

#it then opens the output file from sCNAHitSeq, preprocesses the data and then looks for overlap

#-------------------
# Part One - LOAD BLACKLIST
#--------------------

# information is stored in the black list as:
# Shitty_Area_ID \t Number of samples \t Length | Possible confirmations
#chr4:34786001-34816001	4	30,000	1494D_X_1524A-1494D_X_1249A-1494D_X_1524A-1494D_X_1249A

#Open the Black Liopen REFERENCE, "/panfs/home/arobertson/SCRIPTS/scripts/CancerType_sCNAs_Matrix.txt" or die "couldn't open the refernece file: $!\n";


open BLACKLIST, "/panfs/home/arobertson/DATA/BANLIST.txt" or die "couldn't open the blacklist file: $!\n";

my $BL_ID= '';
my $BL_VAR1 = '';
my $BL_VAR2 = '';
my $BL_VAR3 = '';
my $BL_VAR4 = ''; #CHR
my $BL_VAR5 = ''; #START
my $BL_VAR6 = ''; #STOP
my %BL_HASH = ();
my %BL_CHR = ();
my %BL_START = ();
my %BL_STOP = ();
my %BL_COUNTS = ();
my %BL_SAMPLES = ();

while (<BLACKLIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        #print "$line \";
        $BL_ID = $data[0];		#ID = chr1:1231123-454564564

        if ($BL_ID eq "Banlist") {next;}
	$BL_HASH{$BL_ID} = $line;
	
	@temp_data1 = split  /:/, $BL_ID;
	$BL_VAR4 = $temp_data1[0];
	$BL_CHR{$BL_ID} = $BL_VAR4;
	@temp_data2 = split  /-/, $temp_data1[1];
	$BL_VAR5 = $temp_data2[0];
	$BL_START{$BL_ID} = $BL_VAR5;	
	$BL_VAR6 = $temp_data2[1];
	$BL_STOP{$BL_ID} = $BL_VAR6;
	
	#print "CHR = $BL_VAR4 \t START = $BL_VAR5 \t STOP = $BL_VAR6 \n";

        $BL_VAR1 = $data[1];		#counts =  2
	$BL_COUNTS{$BL_ID} = $BL_VAR1;	

        $BL_VAR2 = $data[2];		#length
        
	$BL_VAR3 = $data[3];		#Samples, 1549A_X_1232D, 1232D_X_1184A
        $BL_SAMPLES{$BL_ID} = $BL_VAR3; 
	#print "ID = $BL_ID \t CHR = $BL_VAR4 \t START = $BL_VAR5 \t STOP = $BL_VAR6 \t No.SAMPLES = $BL_COUNTS{$BL_ID} \n";
}

print "CNVs imported! \n";

#--------------------------------------------------------
# PART 2 - import results
#------------------------------------------------------

open RESULTS, "/panfs/home/arobertson/DATA/WARD_DATA/G340_i_11_i_[null]_i_[null]_e_all.txt_edited_CNV-1.txt" or die "couldn't open the results file: $!\n";

open  OUT, ">/panfs/home/arobertson/DATA/WARD_DATA/WARD_TEST0.txt";
print OUT "ExperimentID\tChr\tFirstProbe\tLastProbe\tNbSnp\tlength_min\tType\tAvg_certainty\tCNV\tSpecificEvents\n";

my %resultsHASH= ();
my $res_ID = '';
my %res2HASH= ();

while (<RESULTS>) {
        my $line = $_;
        chomp $line;
        @bata = split /\s+/, $line;
                if ($bata[0] eq "Sample" ) {next;}
                if ($bata[0] =~ "##" ) {print OUT "$line\n"; next;}
        $RESULTS_CHR = "chr$bata[1]";
        $RESULTS_START = $bata[2];
        $RESULTS_STOP = $bata[3];
	$res_ID = "$RESULTS_CHR:$RESULTS_START-$RESULTS_STOP";
	#print "$line \n"
	#print "RESULTS ID = $res_ID \n";
	
	$resultsHASH{$res_ID} = "Candidate";
	$res2HASH{$res_ID} = "N/A";

	foreach my $Ban_ID (sort keys %BL_HASH) {
		$BanCHR		=	$BL_CHR{$Ban_ID};
		$BanSTART	=	$BL_START{$Ban_ID};
		$BanSTOP	=	$BL_STOP{$Ban_ID};

		 if ($BanCHR eq $RESULTS_CHR ) {} else {next;}

		 #MODE A - if CNV is inside a known peak
                 if ($BanSTART < $RESULTS_START && $BanSTOP > $RESULTS_STOP) {
		 #print "CLASSA\t$Ban_ID\t$line \n";
		 $resultsHASH{$res_ID} = "KnownCNV-ClassA";

		 if ($res2HASH{$res_ID} eq 'N/A'){$res2HASH{$res_ID} = $Ban_ID;} elsif ($res2HASH{$res_ID} eq $Ban_ID){next;} else {$res2HASH{$res_ID} = "$res2HASH{$res_ID},$Ban_ID"}	
		 }

		#MODE B - CNV starts inside but finishes outside
                if ($RESULTS_START > $BanSTART && $RESULTS_START < $BanSTOP && $RESULTS_STOP > $BanSTOP) {
		#print "CLASSB\t$Ban_ID\t$line \n";
		 $resultsHASH{$res_ID} = "KnownCNV-ClassB";
		if ($res2HASH{$res_ID} eq 'N/A'){$res2HASH{$res_ID} = $Ban_ID;} elsif ($res2HASH{$res_ID} eq $Ban_ID){next;} else {$res2HASH{$res_ID} = "$res2HASH{$res_ID},$Ban_ID"} 	
		}
		
		#MODE C - CNV starts before ref but finishes inside
                if ($RESULTS_START < $BanSTART && $RESULTS_STOP > $BanSTART &&  $RESULTS_STOP < $BanSTOP ) {
		 $resultsHASH{$res_ID} = "KnownCNV-ClassC";
		if ($res2HASH{$res_ID} eq 'N/A'){$res2HASH{$res_ID} = $Ban_ID;} elsif ($res2HASH{$res_ID} eq $Ban_ID){next;} else {$res2HASH{$res_ID} = "$res2HASH{$res_ID},$Ban_ID"} 
		#print "CLASSC\t$Ban_ID\t$line \n";
			}

		#MODE D - if CNV engulfs REF peak
         	if ($BanSTART > $RESULTS_START && $BanSTOP < $RESULTS_STOP) {
		#print "CLASSD\t$Ban_ID\t$line \n";
		 $resultsHASH{$res_ID} = "KnownCNV-ClassD";
                if ($res2HASH{$res_ID} eq 'N/A'){$res2HASH{$res_ID} = $Ban_ID;} elsif ($res2HASH{$res_ID} eq $Ban_ID){next;} else {$res2HASH{$res_ID} = "$res2HASH{$res_ID},$Ban_ID"} 
		}

		#print "$line\t $resultsHASH{$res_ID} \n";
	
		}
		
		print OUT "$line\t $resultsHASH{$res_ID} \t $res2HASH{$res_ID}\n";

	}
