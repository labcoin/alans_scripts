#!/usr/bin/perl -w

@data = ();

$NORMAL_POS = '';
$NORMAL_ALT = '';
%NORMAL_HASH = ();
%NORMAL_ALL = ();

%COVERAGE_HASH=();

$NORMAL_LOC="/mnt/coin/arobertson/FINAL2/1524_N1/";
$NORMAL_BAM="1524_N1.sorted.vc2.chr9.vcf";
#$CANCER=""
$CANCER_LOC="/mnt/coin/arobertson/FINAL2/1524_T0/";
$CANCER_BAM="1524_T0.sorted.vc1.chr9.vcf";


open NORMAL_SNVs, "${NORMAL_LOC}${NORMAL_BAM}" or die "couldn't open the results file: $!\n";

open CANCER_SNVs, "${CANCER_LOC}${CANCER_BAM}" or die "couldn't open the results file: $!\n";

#------------------------------------------------------
while (<NORMAL_SNVs>) {
#Produces two hashes
	#1. POS x ALT = NORMAL_HASH 
	#2. POS x LINE= NORAML_ALL

        $line = $_;
	chomp $line;
	@data = split /\t/, $line;

	if($data[0] =~ /#/) {next;}
	if(($data[7]) =~ /INDEL/ ) {next;}
	
	$NORMAL_ALT = $data[4];
	$NORMAL_POS = $data[1];
		#print "$NORMAL_POS \t $NORMAL_ALT \n";
	
	if ($NORMAL_ALT =~ /./) {
		$NORMAL_HASH{$NORMAL_POS}=$NORMAL_ALT;		#-> SNP POSITION = NORMAL in NORMAL
		$NORMAL_ALL{$NORMAL_POS}=$line; 
		#print "$NORMAL_POS \t $NORMAL_HASH{$NORMAL_POS} \n";
			#die;
		} else {
		$COVERAGE_HASH{$NORMAL_POS}=$line;
		}
	}

#foreach $X (sort keys %NORMAL_HASH) {print "$NORMAL_HASH{$X} \n";}

#----------------------------------------------------
@data = ();

$CANCER_POS = '';
$CANCER_ALT = '';
%CANCER_HASH = ();
%CANCER_ALL = ();

while (<CANCER_SNVs>) {
#Produces two hashes
        #1. POS x ALT = CANCER_HASH 
        #2. POS x LINE= CANCER_ALL

        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
		if($data[0] =~ /#/) {next;}
		#if(length ($data[3]) > 1 ) {next;}
		#if(length ($data[4]) > 1 ) {next;}
        	if(($data[7]) =~ /INDEL/ ) {next;}
	$CANCER_ALT = $data[4];
        $CANCER_POS = $data[1];

        $CANCER_HASH{$CANCER_POS}=$CANCER_ALT;
        $CANCER_ALL{$CANCER_POS}=$line;
        }

#--------------------------------------------

my @CANCER_SPECIFIC_HASH = ();
my @NORMAL_SPECIFIC_HASH = ();

#--------------------------------

foreach my $CANCER_SNP (sort keys %CANCER_HASH) {	#for each of the events in 
	#$ALT = $CANCER_HASH{$CANCER_SNP};
	
	if (exists $NORMAL_HASH{$CANCER_SNP}) {		
	#print "POS=$CANCER_SNP\t NORMAL=$NORMAL_HASH{$CANCER_SNP}\tTUMOUR=$CANCER_HASH{$CANCER_SNP}\n";
		if ($NORMAL_HASH{$CANCER_SNP} ne $CANCER_HASH{$CANCER_SNP}){
		my $NORMAL_LINE = $NORMAL_ALL{$CANCER_SNP};
		@data = split /\t/, $NORMAL_LINE;
		@xata = split /;/, $data[7];
		if ($xata[3] =~ /DP4/) {$DP4 = $xata[3];} elsif ($xata[3] =~ /FQ/){$DP4 = "N/A"; next;}  
		@zata = split /,/ $DP4;
		$SCORE_1 = $zata[2] + $zata[3];		

		 my $TUMOUR_LINE = $CANCER_ALL{$CANCER_SNP};
                #print "$TUMOUR_LINE \n";
		@tata = split /\t/, $TUMOUR_LINE;
                @cata = split /;/, $tata[7];
		
		print "POS=$CANCER_SNP\t REF=$data[3]\tNORMAL=$NORMAL_HASH{$CANCER_SNP}\tSCORE=$SCORE_1\t$xata[0]\t$xata[3]\tTUMOUR=$CANCER_HASH{$CANCER_SNP}\t$cata[0]\t$cata[3]\n";
		}
		$CANCER_SPECIFIC_HASH{$CANCER_SNP} = "$CANCER_ALL{$CANCER_SNP}\t$NORMAL_HASH{$CANCER_SNP}";}
	}

#----------------------------------------------------------
# need two hashes to end
# here's where the magic happended
#----------------------------------------------------------

open TUMOUR_RESULTS, ">TRYAGAIN_TUMOUR_SPECIFIC_SNPS.txt";

print TUMOUR_RESULTS "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n";

foreach my $TS_SNPs (sort keys %CANCER_SPECIFIC_HASH) {
	print TUMOUR_RESULTS "$CANCER_SPECIFIC_HASH{$TS_SNPs} \n";
	}
	

#open NORMAL_RESULTS, ">VC2x_NORMAL_SPECIFIC-SNPS-SUMMARY.txt";

#print NORMAL_RESULTS "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n";

#foreach my $NS_SNPs (sort keys %NORMAL_SPECIFIC_HASH) {
#        print NORMAL_RESULTS "$NORMAL_SPECIFIC_HASH{$NS_SNPs} \n";
#        }


