#!/usr/bin/perl -w

@data = ();

$NORMAL_POS = '';
$NORMAL_ALT = '';
%NORMAL_HASH = ();
%NORMAL_ALL = ();

$NORMAL_LOC="/mnt/coin/arobertson/FINAL2/1249_N1/";
$NORMAL_BAM="1249_N1.sorted.vc1.chr9.vcf";
#$CANCER=""
$CANCER_LOC="/mnt/coin/arobertson/FINAL2/1524_N1/";
$CANCER_BAM="1524_N1.sorted.vc1.chr9.vcf";

#$CANCER_LOC="/mnt/coin_arobertson/FINAL/1524_T/";
#$CANCER_BAM="1524_T.sorted.chr9.vcf";


open NORMAL_SNVs, "${NORMAL_LOC}${NORMAL_BAM}" or die "couldn't open the normal file: $!\n";

open CANCER_SNVs, "${CANCER_LOC}${CANCER_BAM}" or die "couldn't open the cancer file: $!\n";

#------------------------------------------------------
while (<NORMAL_SNVs>) {
#Produces two hashes
	#1. POS x ALT = NORMAL_HASH 
	#2. POS x LINE= NORAML_ALL

        $line = $_;
	chomp $line;
	@data = split /\t/, $line;

	if($data[0] =~ /#/) {next;}
	if(length $data[3] > 1.1) {next;}
	if(length $data[4] > 1.1) {next;}
	if($data[7] =~ /INDEL/) {next;}	
	if($data[5] < 5) {next;}
	if($data[4] =~ /\./ ) {next;} #if the normal snp isn't detected, tumour and not a snp

	$NORMAL_ALT = $data[4];
	$NORMAL_POS = $data[1];
		#print "$NORMAL_POS \t $NORMAL_ALT \n";
		
	$NORMAL_HASH{$NORMAL_POS}=$NORMAL_ALT;
	$NORMAL_ALL{$NORMAL_POS}=$line; 
		#print "$NORMAL_POS \t $NORMAL_HASH{$NORMAL_POS} \n";
	#die;
	}

#foreach $X (sort keys %NORMAL_HASH) {print "$NORMAL_HASH{$X} \n";}

#----------------------------------------------------
@data = ();

$CANCER_POS = '';
$CANCER_ALT = '';
%CANCER_HASH = ();
%CANCER_ALL = ();

while (<CANCER_SNVs>) {
#Produces two hashes
        #1. POS x ALT = CANCER_HASH 
        #2. POS x LINE= CANCER_ALL

        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
		
		if($data[0] =~ /#/) {next;}
        	if(length $data[3] > 1.1) {next;}
        	if(length $data[4] > 1.1) {next;}
        	if($data[7] =~ /INDEL/) {next;}
        
	$CANCER_ALT = $data[4];
        $CANCER_POS = $data[1];

        $CANCER_HASH{$CANCER_POS}=$CANCER_ALT;
        $CANCER_ALL{$CANCER_POS}=$line;
        }

#=========================

my %SHARED_SNP =();

$TOTAL=0;
$SAME=0;
$DIF=0;
$LOH=0;

foreach my $NORMAL_SNP (sort keys %NORMAL_HASH) {
	$TOTAL++;
		if (exists $CANCER_HASH{$NORMAL_SNP}) {
		#print "SNP: $NORMAL_SNP\tNORM:$NORMAL_HASH{$NORMAL_SNP}\tTUM:$CANCER_HASH{$NORMAL_SNP}\n";
		if ($NORMAL_HASH{$NORMAL_SNP} eq $CANCER_HASH{$NORMAL_SNP}) {$SAME++; $SHARED_SNP{$NORMAL_SNP}="$NORMAL_ALL{$NORMAL_SNP}\t"} else {$DIF++;}
	}else{
		#print "SNP: $NORMAL_SNP\tNORM:$NORMAL_HASH{$NORMAL_SNP}\tTUM:X\n";
		$LOH++;
	}
}

#print "TOTAL = $TOTAL\tSAME = $SAME\tDIF = $DIF\tLOH = $LOH \n";

#--------------------------------------------
=cut
my @CANCER_SPECIFIC_HASH = ();
my @NORMAL_SPECIFIC_HASH = ();

#--------------------------------

#foreach my $CANCER_SNP (sort keys %CANCER_HASH) {	#for each of the events in 
#	$ALT = $CANCER_HASH{$CANCER_SNP};
#	
#	if (exists $NORMAL_HASH{$CANCER_SNP}) {		#if the CANCER SNP exists in the normal sample,	
#		next;} else {				#don't do anything, but if it doesn't
#		$CANCER_SPECIFIC_HASH{$CANCER_SNP} = $CANCER_ALL{$CANCER_SNP};}
#	}


foreach my $NORMAL_SNP (sort keys %NORMAL_HASH) {      
	if 

        if (exists $CANCER_HASH{$NORMAL_SNP}) {       
                next;} else {                        
                $NORMAL_SPECIFIC_HASH{$NORMAL_SNP} = $NORMAL_ALL{$NORMAL_SNP};}
        }


#----------------------------------------------------------
# need two hashes to end
# here's where the magic happended
#----------------------------------------------------------

open TUMOUR_RESULTS, ">TUMOUR_SPECIFIC-SNPS-SUMMARY.txt";

print TUMOUR_RESULTS "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n";

foreach my $TS_SNPs (sort keys %CANCER_SPECIFIC_HASH) {
	print TUMOUR_RESULTS "$CANCER_SPECIFIC_HASH{$TS_SNPs} \n";
	}
	

open NORMAL_RESULTS, ">NORMAL_SPECIFIC-SNPS-SUMMARY.txt";

print NORMAL_RESULTS "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n";

foreach my $NS_SNPs (sort keys %NORMAL_SPECIFIC_HASH) {
        print NORMAL_RESULTS "$NORMAL_SPECIFIC_HASH{$NS_SNPs} \n";
        }


