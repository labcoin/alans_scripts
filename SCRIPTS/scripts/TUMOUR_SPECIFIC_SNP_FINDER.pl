#!/usr/bin/perl -w


#-------------------------------------------------------
# Tumour specific SNP finder
#  Scrabbled together by Alan on the 19th of July 2015
#
# Running the script
#  1. Enter the location of the vc1 and vc2 files 
#  2. Update the normal_limit / chr name
#  3. :wq!
#  4. perl this_script.pl
#
# Outputs
#  1. Summary file .txt
#  2. A VCF file containing all the tumour specific 
#
#-------------------------------------------------------

#Enter the location of the VC1 file
$CANCER_LOC="/mnt/coin/arobertson/FINAL2/1524_T0/";
#Enter the name of the VC1 file
$CANCER_VCF="1524_T0.sorted.vc1.chr9.vcf";

#Enter the location of the VC2 file
$NORMAL_LOC="/mnt/coin/arobertson/FINAL2/1524_N1/";
#Enter the name of the VC2 file
$NORMAL_VCF="1524_N1.sorted.vc2.chr9.vcf";

open CANCER_SNVs, "${CANCER_LOC}${CANCER_VCF}" or die "couldn't open the VC1 file: $!\n";
open NORMAL_SNVs, "${NORMAL_LOC}${NORMAL_VCF}" or die "couldn't open the VC2 file: $!\n";

#Enter the minimum number of reads covering each of these snps in the normal
$NORMAL_LIMIT="4";

#Enter the chromosome
$A_CHR="CHR_9";

#---------------------------------------
#setup up varibables
@data = ();

$NORMAL_POS = '';
$NORMAL_ALT = '';
%NORMAL_HASH = ();
%NORMAL_ALL = ();

%COVERAGE_HASH=();

#-------------------------------------

while (<NORMAL_SNVs>) {
#Produces two hashes
	#1. POS x ALT = NORMAL_HASH 
	#2. POS x LINE= NORAML_ALL

        $line = $_;
	chomp $line;
	@data = split /\t/, $line;

	if($data[0] =~ /#/) {next;}
	if(($data[7]) =~ /INDEL/ ) {next;}
	
	$NORMAL_ALT = $data[4];
	$NORMAL_POS = $data[1];
		#print "$NORMAL_POS \t $NORMAL_ALT \n";
	
	if ($NORMAL_ALT =~ /./) {
		$NORMAL_HASH{$NORMAL_POS}=$NORMAL_ALT;		#-> SNP POSITION = NORMAL in NORMAL
		$NORMAL_ALL{$NORMAL_POS}=$line; 
		#print "$NORMAL_POS \t $NORMAL_HASH{$NORMAL_POS} \n";
			#die;
		} else {
		$COVERAGE_HASH{$NORMAL_POS}=$line;
		}
	}

#foreach $X (sort keys %NORMAL_HASH) {print "$NORMAL_HASH{$X} \n";}

#----------------------------------------------------
@data = ();

$CANCER_POS = '';
$CANCER_ALT = '';
%CANCER_HASH = ();
%CANCER_ALL = ();

while (<CANCER_SNVs>) {
#Produces two hashes
        #1. POS x ALT = CANCER_HASH 
        #2. POS x LINE= CANCER_ALL

        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
		if($data[0] =~ /#/) {next;}
		#if(length ($data[3]) > 1 ) {next;}
		#if(length ($data[4]) > 1 ) {next;}
        	if(($data[7]) =~ /INDEL/ ) {next;}
	$CANCER_ALT = $data[4];
        $CANCER_POS = $data[1];

        $CANCER_HASH{$CANCER_POS}=$CANCER_ALT;
        $CANCER_ALL{$CANCER_POS}=$line;
        }

#--------------------------------------------

my @CANCER_SPECIFIC_HASH = ();
my @NORMAL_SPECIFIC_HASH = ();
my %FINAL_HASH=();

#--------------------------------

foreach my $CANCER_SNP (sort keys %CANCER_HASH) {	#for each of the events in 
	#$ALT = $CANCER_HASH{$CANCER_SNP};
	
	if (exists $NORMAL_HASH{$CANCER_SNP}) {		
	#print "POS=$CANCER_SNP\t NORMAL=$NORMAL_HASH{$CANCER_SNP}\tTUMOUR=$CANCER_HASH{$CANCER_SNP}\n";
		if ($NORMAL_HASH{$CANCER_SNP} ne $CANCER_HASH{$CANCER_SNP}){
		my $NORMAL_LINE = $NORMAL_ALL{$CANCER_SNP};
		@data = split /\t/, $NORMAL_LINE;
		@xata = split /;/, $data[7];
		if ($xata[3] =~ /DP4/) {$DP4 = $xata[3];} elsif ($xata[3] =~ /FQ/){$DP4 = "N/A"; next;}  
		@zata = split /,/, $DP4;
		$SCORE_1 = $zata[2] + $zata[3];		
	#	$SCORE_0 = $zata[2] + $zata[3] + $zata[4] + $zata[1];
		@ZATA = split /=/, $zata[0];
		$SCORE_0 = $zata[2] + $zata[3] + $ZATA[1] + $zata[1];
		@XATA = split /=/, $xata[0];



		 my $TUMOUR_LINE = $CANCER_ALL{$CANCER_SNP};
                #print "$TUMOUR_LINE \n";
		@tata = split /\t/, $TUMOUR_LINE;
                @cata = split /;/, $tata[7];
		@rata = split /,/, $cata[3];	
		@RATA = split /=/, $rata[0];
		$TUMOUR_SCORE= $rata[2] + $rata[3];
		$TUMOUR_SCORE0= $rata[2] + $rata[3] + $RATA[1] + $rata[1];
		@CATA = split /=/, $cata[0];	
	
		$out_chr=$data[0];
		$out_pos=$CANCER_SNP;
		$out_ref=$data[3];
		$out_nSNP=$NORMAL_HASH{$CANCER_SNP};
		$out_nDP=$XATA[1];
		$out_nREADs=$SCORE_0;
		$out_nMM=$SCORE_1;
		$out_tSNP=$tata[4];
		$out_tDP=$CATA[1];
		$out_tREADs=$TUMOUR_SCORE0;
		$out_tREADs1=$TUMOUR_SCORE;
		

if (($out_nMM == 0) && ($out_nREADs > $NORMAL_LIMIT)) {
#print "$out_chr\t$out_pos\t$out_ref\t$out_nSNP\t$out_nDP\t$out_nREADs\t$out_nMM\t$out_tSNP\t$out_tDP\t$out_tREADs\t$out_tREADs1\n";

$FINAL_HASH{$CANCER_SNP}="$out_chr\t$out_pos\t$out_ref\t$out_nSNP\t$out_nDP\t$out_nREADs\t$out_nMM\t$out_tSNP\t$out_tDP\t$out_tREADs\t$out_tREADs1";

$CANCER_SPECIFIC_HASH{$CANCER_SNP} = "$CANCER_ALL{$CANCER_SNP}";

}	
		#$FINAL_HASH{$CANCER_SNP}="$data[0]\t$CANCER_SNP\t$data[3]\t$NORMAL_HASH{$CANCER_SNP}\t$SCORE_1\t$xata[0]\t$xata[3]\t$tata[4]\t$cata[0]\t$cata[3]\n";
		}
		#$CANCER_SPECIFIC_HASH{$CANCER_SNP} = "$CANCER_ALL{$CANCER_SNP}\t$NORMAL_HASH{$CANCER_SNP}";}
	}
}
#----------------------------------------------------------
# need two hashes to end
# here's where the magic happended
#----------------------------------------------------------

$File_Name = "FINAL_TUMOUR_SNPS_$A_CHR";

open TUMOUR_RESULTS, ">$File_Name.txt";
open TUMOUR_VCF, ">$File_Name.vcf";

print TUMOUR_RESULTS "Chr\tPosition\tRef\tNormal_SNP\tTotal_Normal_Reads1\tTotal_Normal_Reads2\tNo.Non_Ref\tTumour_SNP\tTumour_Normal_Reads1\tTumour_Normal_Reads2\tSupporting_Tumour_SNPS\n";

foreach my $TS_SNPs (sort keys %CANCER_SPECIFIC_HASH) {
	print TUMOUR_RESULTS "$FINAL_HASH{$TS_SNPs}\n";
	print TUMOUR_VCF "$CANCER_SPECIFIC_HASH{$TS_SNPs}\n";

	}
	
print "Tumour specific snps identified, now go do something amazing with these result \n";


#open NORMAL_RESULTS, ">VC2x_NORMAL_SPECIFIC-SNPS-SUMMARY.txt";

#print NORMAL_RESULTS "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n";

#foreach my $NS_SNPs (sort keys %NORMAL_SPECIFIC_HASH) {
#        print NORMAL_RESULTS "$NORMAL_SPECIFIC_HASH{$NS_SNPs} \n";
#        }


