#!/usr/bin/perl -w

#This script has been written to process the output from Lachlan's overlap script and identify the genes inside.

#-------------------
# Part One - Load results from overlap
#--------------------

# information is stored in the results  as:
# type	test	reference	chr	st(Mb)	end(Mb)	type	len(Mb)

open OVERLAP_LIST, "/panfs/home/arobertson/DATA/G252_065x1084.csv" or die "couldn't open the blacklist file: $!\n";

#use MATH::Round;

my %OL_HASH = ();
my %OL_CHR = ();
my %OL_START = ();
my %OL_STOP = ();
my %OL_TYPE= ();

while (<OVERLAP_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
		if ($data[0] eq "type" ) {print "Starting analysis \n"; next;}
	$temp_start=$data[4];
	$temp_start=$temp_start*1000000;
	$temp_start = sprintf "%.0f", $temp_start;
	
	$temp_stop=$data[5];
	$temp_stop=$temp_stop*1000000;
	$temp_stop = sprintf "%.0f", $temp_stop;
	#round($temp_start);
	#print "$temp_start\n";
	#print "chr : $data[3] start $data[4] \t stop $data[5] \t type $data[6]\n";
	$temp_ID="chr$data[3]:$temp_start-$temp_stop";
	#print "$temp_ID \n";

	$OL_HASH{$temp_ID}	=	$line;
	$OL_CHR{$temp_ID}	=	$data[3];
	$OL_START{$temp_ID}	=	$temp_start;
	$OL_STOP{$temp_ID}  	=       $temp_stop;
	$OL_TYPE{$temp_ID} 	= 	$data[6];

}

#--------------------
# Part two
#--------------------

open GENE_LIST, "/panfs/home/arobertson/DATA/ENSEMBL_GENELIST.txt" or die "couldn't open the results file: $!\n";

	%COUNTS = ();
	%GENES_PER_CNV = ();
	%SYM_PER_CNV = ();

	%GENE_HASH = ();
	%GENE_DATA = ();
	%GENE_C2 = ();
	%TYPE_HASH2 = ();

while (<GENE_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;

	#print "$data[0]  \t $data[1] \n";
	$GENE_ID=$data[0];
	if ($data[0] eq "Ensembl Gene ID" ) {next;}
	$GENE_SYM=$data[1];
	if ($data[2] =~ /^[0-9,.E]+$/) {} else {next;}
	$GENE_CHR=$data[2];
	$GENE_START=$data[3];
	$GENE_STOP=$data[4];
	$GENE_LOC="chr$GENE_CHR:$GENE_START-$GENE_STOP";
	$GENE_DATA{$GENE_SYM}="$data[6]\t$data[8]\t$GENE_ID\t$GENE_LOC";
	$GENE_COUNT=0;
	$GENE_COUNT2=0;


        foreach my $CNV_ID (sort keys %OL_HASH) { #for each CNV location
                $CNV_CHR         =       $OL_CHR{$CNV_ID};
                $CNV_START       =       $OL_START{$CNV_ID};
                $CNV_STOP        =       $OL_STOP{$CNV_ID};
		$CNV_TYPE	 =	 $OL_TYPE{$CNV_ID};
		#$SYM_PER_CNV{$CNV_ID} = "N/A";

		if ($CNV_CHR == $GENE_CHR ) {} else {next;}

		 #MODE A - if gene is inside a CNV
                 if ($CNV_START < $GENE_START && $CNV_STOP > $GENE_STOP) {
                	if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}	
			 $GENE_COUNT++;
			 $COUNTS{$CNV_ID}=$GENE_COUNT;
			if (exists $SYM_PER_CNV{$CNV_ID}) {  
			$SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(I)"}
			else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(I)"}
			
			if (exists $GENE_HASH{$GENE_SYM}) {	
			$GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(I)"}
			else {
			$GENE_HASH{$GENE_SYM}="$CNV_ID.(I)"; 
			$TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }	;	 
		
			if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};} 
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;

		#print "$GENE_SYM\t is inside $CNV_ID \n";
                 #;
		} #close MODE A
		
		#Start MODE B
		if ($GENE_START > $CNV_START && $GENE_START < $CNV_STOP && $GENE_STOP > $CNV_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(P3)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(P3)"} ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(P3)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(P3)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;

		} #close MODEB	

                #Start MODE C
                if ($GENE_START < $CNV_START && $GENE_STOP > $CNV_START &&  $GENE_STOP < $CNV_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(5P)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(5P)"} ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(5P)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(5P)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;
                } #close MODEC  

                 #MODE D - if gene is spans CNV
                 if ($CNV_START > $GENE_START && $CNV_STOP < $GENE_STOP) {
                        if (exists $COUNTS{$CNV_ID}) {$GENE_COUNT=$COUNTS{$CNV_ID};}
                         $GENE_COUNT++;
                         $COUNTS{$CNV_ID}=$GENE_COUNT;
                        if (exists $SYM_PER_CNV{$CNV_ID}) {
                        $SYM_PER_CNV{$CNV_ID}="$SYM_PER_CNV{$CNV_ID},$GENE_SYM.(S)"}
                        else {$SYM_PER_CNV{$CNV_ID} = "$GENE_SYM.(S)"}  ;

                        if (exists $GENE_HASH{$GENE_SYM}) {
                        $GENE_HASH{$GENE_SYM}="$GENE_HASH{$GENE_SYM},$CNV_ID.(S)"}
                        else {
                        $GENE_HASH{$GENE_SYM}="$CNV_ID.(S)";
                        $TYPE_HASH2{$GENE_SYM} = $CNV_TYPE;  }  ;

                        if (exists $GENE_C2{$GENE_SYM}) {$GENE_COUNT2=$GENE_C2{$GENE_SYM};}
                        $GENE_COUNT2++;
                        $GENE_C2{$GENE_SYM}=$GENE_COUNT2;
                #print "$GENE_SYM\t is inside $CNV_ID \n";
                 #;
                } #close MODE A


		} #close CNV_ID look	

}

open  OUT, ">/panfs/home/arobertson/DATA/WARD_DATA/G252_OVERLAP.txt";
print OUT "CNV\tTYPE\tNo.Genes\tGenes\n";
		
	foreach my $ID (sort keys %COUNTS) { 
		print OUT "$ID\t$OL_TYPE{$ID}\t$COUNTS{$ID}\t$SYM_PER_CNV{$ID}\n";
		}

open  OUT1, ">/panfs/home/arobertson/DATA/WARD_DATA/G252_OVERLAP_GENES.txt";
		
		print OUT1"Gene\tGene-Type\tStatus\tEnsemblID\tLocation\tCNV-type\tNo.CNVs\tCNVs\n";

        foreach my $GENES (sort keys %GENE_HASH) {
                print OUT1 "$GENES\t$GENE_DATA{$GENES}\t$TYPE_HASH2{$GENES}\t$GENE_C2{$GENES}\t$GENE_HASH{$GENES}\n";
                }

print "CNVs annotated \n";
