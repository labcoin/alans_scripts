#!/usr/bin/perl -w

#----------
# This script has been written to compare the SNVs identified in 1249,1494 and 1524 to those identifed in the 609 and 616 

#----------

#logic
#make id for each SNP = chrXX_LOCATION_wt_SNP
#I know I don't need the wild type allele, but I'm using it as a check
#for each file store ID in combined has and individual hash

# Part One - Variables 

%COMBINED_HASH=();
%HASH_1524=();
%HASH_1494=();
%HASH_1249=();
%HASH_616=();
%HASH_609=();

@data=();
$ID=();

# Part two
open FILE_1524, "/panfs/home/arobertson/TRANSFER/Plasma_VCFs/1524_P0_unique.filtered.vcf.snp.txt" or die "couldn't open the GENELIST file: $!\n";

while (<FILE_1524>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
#	print "$ID \n";
	$COMBINED_HASH{$ID}="1";
	$HASH_1524{$ID}="$data[10]";
	}

close FILE_1524;
print "1524 read \n";

####1494####

open FILE_1494, "/panfs/home/arobertson/TRANSFER/Plasma_VCFs/1494_P0_unique.filtered.vcf.snp.txt" or die "couldn't open the 1494 file: $!\n";

while (<FILE_1494>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
#       print "$ID \n";
        $COMBINED_HASH{$ID}="1";
        $HASH_1494{$ID}="$data[10]";
        }

close FILE_1494;
print "1494 read \n";

####1494####

open FILE_1249, "/panfs/home/arobertson/TRANSFER/Plasma_VCFs/1249_P0_unique.filtered.vcf.snp.txt" or die "couldn't open the 1249 file: $!\n";

while (<FILE_1249>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
#       print "$ID \n";
        $COMBINED_HASH{$ID}="1";
        $HASH_1249{$ID}="$data[10]";
        }

close FILE_1249;
print "1249 read \n";


###NORMALS

open FILE_609, "/panfs/home/arobertson/TRANSFER/065_Px.filtered.vcf.snp" or die "couldn't open the 609 file: $!\n";

while (<FILE_609>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
        $COMBINED_HASH{$ID}="1";
        $HASH_609{$ID}="$data[10]";
        }

close FILE_609;
print "609 read \n";

open FILE_616, "/panfs/home/arobertson/TRANSFER/098_Px.filtered.vcf.snp" or die "couldn't open the 616 file: $!\n";

while (<FILE_616>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
        $ID="$data[0]"."_"."$data[1]"."_"."$data[2]"."_"."$data[3]";
        $COMBINED_HASH{$ID}="1";
        $HASH_616{$ID}="$data[10]";
        }

close FILE_616;
print "616 read \n";


###### FILL IN EMPTY VALUES

foreach $X (sort keys %COMBINED_HASH) {
	if (exists $HASH_1524{$X}) {} else {$HASH_1524{$X}="N/A";} 
	if (exists $HASH_1494{$X}) {} else {$HASH_1494{$X}="N/A";}
	if (exists $HASH_1249{$X}) {} else {$HASH_1249{$X}="N/A";}
	if (exists $HASH_616{$X}) {} else {$HASH_616{$X}="N/A";}
	if (exists $HASH_609{$X}) {} else {$HASH_609{$X}="N/A";}
}

##### PRINT OUT
open  OUT, ">/panfs/home/arobertson/TRANSFER/Plasma_VCFs/REDONE.txt";
print OUT "SNV_ID\t1524P0\t1494P0\t1249T\t609P0\t616P0\n";

foreach $Y (sort keys %COMBINED_HASH) {
print OUT "$Y\t$HASH_1524{$Y}\t$HASH_1494{$Y}\t$HASH_1249{$Y}\t$HASH_609{$Y}\t$HASH_616{$Y}\n";
	}
