#!/usr/bin/perl -w
use strict;

#---------------------------------
# Convert sCNA changes  
# This script has been written to convert the new way of recording sCNA changes in 0.7 (1 = 0.5, 2=1, 3=1.5, etc), into a format that can be compared wothe array data


# Introduction of QC 
# The second version of this script performs 
# It generates a second summary file 
# In order to achieve this, it requires the cumr 
# Determine the number 
#--------------------------------
=cut

Current output
Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
EHLC_FETAL01vM4       23 43000001 46800000      10  3.80e+06 0.500  1.00
EHLC_FETAL01vM4       1 20207003 22507001      20  2.30e+06  2.00  1.00

EHLC_FETAL01vM4	X	43000001	46800000	10	7600000	0.500
EHLC_FETAL01vM4	1	20207003	22507001	20	4600000	2.00

Transform
1. Replace chromosome X with chromosme 23
2. Double Type
3. Replace format?
4. MAKE PRODUCTION READY
=cut

my $LOCATION_NAME=$ARGV[0];
my $FILE_NAME=$ARGV[1];

my @data = ();		#array containing raw data
my $sampleA = '';	#stores counts from SampleA
my $sampleB = '';	#stores counts from SampleB
my $length = 0;	#Determine the legnth of the CNVs
my $running = 0;	
my $counts = 0;		#count the number of CNVs
my $format = '';	#variable defining format
my $chr = '';

my $loc="$LOCATION_NAME";
my $name="$FILE_NAME";
my $new_name="_edited_CNV.txt";
my $QC="QC.txt";

open (INPUT,"$loc$name") or die "couldn't open the results file: $!\n";

open  OUT,">$loc$new_name";
open  OUT2,">$loc$QC";

while (<INPUT>) {
        my $line = $_;
        chomp $line;
        @data = split /\s+/, $line;

	if ($data[0] eq 'Sample')					#remove qCNV header and replace with qBAMheader
		{	print OUT "Sample\tChr\tFirstProbe\tLastProbe\tNbSnp\tlength_min\tType\tAvg_certainty\n";
			#print OUT "Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty\n";
			next;}

	if ($data[0] =~/##/) {print OUT "$line\n"; print OUT2 "$line\n"; next;}
	#convert X to 23
	$chr = $data[1];
	 if ($chr eq 'X') {$chr = '23';}
		

	#double CNV
	$format == $data[6];
	if ($format = 1.0) {
		$sampleB = $data[6] + $data[6];	

	#QC
		$counts++; #increase the number of CNVs identified
		$length = $data[3]-$data[2];
		$running = $length + $running;
	
		print OUT "$data[0]\t$chr\t$data[2]\t$data[3]\t$data[4]\t$length\t$sampleB\t$data[7]\n";
		}
}
	my $average = 0;
	$average = $running / $counts;

	if ($counts > 1000) {print OUT2 "WARNING - THIS SAMPLE HAS FAILED QC \n";}

	print OUT2 "Number of CNVs:\t$counts\nAverage Length of CNV:\t$average\n";	

print "Reformating complete \n";
