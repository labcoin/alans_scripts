#!/usr/bin/perl -w

# This script processes the matching CNVs file and removes duplications 
# Pulls conserved CNVs regions
#

$name="G339_065x1084";

open (OVERLAP_LIST,"/panfs/home/arobertson/DATA/WARD_DATA/RAW_OVERLAP/$name.csv") or die "couldn't open the results file: $!\n";

%OL_HASH = ();
%OL_CHR = ();
%OL_START = ();
%OL_STOP = ();
%OL_TYPE= ();

while (<OVERLAP_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
                if ($data[0] eq "type" ) {print "$line \n"; next;}

	$temp_start=$data[4];
        $temp_start=$temp_start*1000000;
        $temp_start = sprintf "%.0f", $temp_start;

        $temp_stop=$data[5];
        $temp_stop=$temp_stop*1000000;
        $temp_stop = sprintf "%.0f", $temp_stop;

	$temp_ID="chr$data[3]:$temp_start-$temp_stop"; 	#this step removes duplications - every CNVs get's an ID. Same location = same ID

}
