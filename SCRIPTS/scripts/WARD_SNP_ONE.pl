#!/usr/bin/perl -w

#----------
# This script has been written to turn the vsqr.vep.vcf into something more human readable 
#----------

open DATA, "/mnt/coin/arobertson/dx_data/variants/R_160210_MATSLO1_M001.hc.vqsr.vep.vcf" or die "couldn't open the GENELIST file: $!\n";

@data=();
@DUMP=();
$TOTAL='';
$BIG_COUNT=0;
%TEST_HASH=();

while (<DATA>) {
        $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
	if ($data[0] =~ /##/) {next;}	#This is the FILE header restore once we've solved the issue
	if ($data[0] =~ /#/) {next;} 	#This is the column header
	$dump = $data[7];
	@DUMP = split  /\|/, $dump;
	$TOTAL= scalar @DUMP;
	$VAR=3;
	$VAR2=0;
	$COUNT=1;
	$SNP_ID="$data[0]_$data[1]";

	
	while ($VAR < $TOTAL) {
	#print "$data[0] $data[1] \n";
	$VAR2=$VAR+7;
	$VAR1=$VAR+1;
	$FULL_ID="$SNP_ID"."_$DUMP[$VAR]";
	if ($DUMP[$VAR2] =~ /protein/) {
		$TEST_HASH{$FULL_ID}="$SNP_ID\t$DUMP[$VAR]\t$DUMP[$VAR1]\t$DUMP[$VAR2]\t$data[2]\t$data[3]\t$data[4]\t$data[5]\t$data[6]\t$data[8]\t$data[9]\t$data[10]\t$data[11]\t$data[12]\t$data[13]\t$data[14]\t$data[15]\t$data[16]\t$data[17]\t$data[18]\t$data[19]\t$data[20]\t$data[21]\t";
		#print "$SNP_ID\t$COUNT:\t$DUMP[$VAR]\t$DUMP[$VAR2] \n";
		$VAR=$VAR+30;
		$COUNT++;} else {$VAR=$VAR+30;}
		}
	#die;
$BIG_COUNT++;

if ($BIG_COUNT == 1000000 || $BIG_COUNT == 2000000 || $BIG_COUNT == 3000000 || $BIG_COUNT == 4000000 || $BIG_COUNT == 5000000 || $BIG_COUNT == 6000000 || $BIG_COUNT == 7000000 || $BIG_COUNT == 8000000 || $BIG_COUNT == 9000000 || $BIG_COUNT == 10000000) {
	print "";	}
}

print "SNP-ID\tENSG-ID\tGENE-SYMBOL\tEFFECT\tdbSNP-ID\tREF-ALLELE\tALT-ALLELE\tQUAL\tFILTER\tFORMAT\t17-2098\tG252\tG272\tG273\tG278\tG302\tG309\tG339\tG340\tG348\tG325\t17-2003\tG365\n";

foreach $A (sort keys %TEST_HASH){
	print "$TEST_HASH{$A} \n";
}
