#!/usr/bin/perl -w

#This script has been written to take a gene list, and extract the location of these genes, and look in other samples, be they normalxnormal or other SameSamplexOtherNormal and determine if the same genes have also been altered

# running the script
# perl 5ALT.pl {Sample_NAME}

# Requirements
# GENELIST output from 6CNVDEX.pl

#Change log
# we now want to ask if these genes contain are altered by CNVs commonly found in the population
# to capture promoter regions we are now looking 10kb upstream and down stream of the genes for CNVs


#-------------------
# Part One - Load results from overlap
#--------------------

# information is stored in the results  as:
# type	test	reference	chr	st(Mb)	end(Mb)	type	len(Mb)

$SAMPLE_NAME=$ARGV[0];
$name="$SAMPLE_NAME";
$file_name="$name"."GENE_LEVEL.txt";
$final_name="$name"."FINAL.txt";
#print "$name \n";
@temp_data=split /065/, $name;
$ward_name=$temp_data[0];
chop $ward_name;
#print "$ward_name \n";
$alt_con="NEW_$ward_name"."v098_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt";

open GENE_LIST, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$file_name" or die "couldn't open the GENELIST file: $!\n";

my %GL_HASH = ();	#ENSG = $line
my %GL_CHR = ();	#ENSG = chrx	
my %GL_START = ();	#ENSG = 444545344
my %GL_STOP = ();	#ENSG = 444545344
my %OL_TYPE= ();

%CON1_HASH=();
%CON2_HASH=();
%CON3_HASH=();
%CON4_HASH=();
%CON5_HASH=();
%CON6_HASH=();

%CNV_GAIN_HASH=();
%CNV_LOSS_HASH=();
%VS098_HASH=();


while (<GENE_LIST>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
		if ($data[0] eq "Gene" ) {print "Starting analysis \n"; next;}
	$ID=$data[1];
	$GL_HASH{$ID}="$line";
	$temp_LOC=$data[5];				#Changed to examine expanded location
		@ddata = split  /:/, $temp_LOC;
		@cdata= split  /chr/, $ddata[0];
		$CHR = $cdata[1];
		@edata = split  /-/, $ddata[1];
		$START="$edata[0]";
		$STOP="$edata[1]";
		#print "$ID\t$CHR\t$START\t$STOP\n";

	$GL_CHR{$ID}="$CHR";
	$GL_STOP{$ID}="$STOP";
	$GL_START{$ID}="$START";

}

#-----------------------------------
# CNV frequency 
#----------------------------------
# Open files containing the the number of times CNVs are commonly seen in populations  

open CNV_GAIN, "/panfs/home/arobertson/DATA/WARD_DATA/supporting_variants_for_nstd46.csv.gz.copy_number_gain" or die "couldn't open the cnv_gain file: $!\n";

while (<CNV_GAIN>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line; 
        $CNV_CHR="$data[0]";
                if ($CNV_CHR eq 'Chr') {next;}
        	if ($CNV_CHR eq 'X') {$CNV_CHR=23;}
		if ($CNV_CHR eq 'Y') {$CNV_CHR=24;}
	$CNV_START="$data[1]";
        $CNV_STOP="$data[2]";
	$CNV_ID="chr$CNV_CHR:$CNV_START-$CNV_STOP";
	$CNV_FREQ="$data[87]";
	$CNV_FREQ=$CNV_FREQ / 2062;
	$CNV_FREQ=$CNV_FREQ*100;
	$CNV_FREQ = sprintf "%.2f", $CNV_FREQ;

	#print "$CNV_ID\t$CNV_FREQ\n";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CNV_GAIN_HASH{$ENS_ID}) {} else {$CNV_GAIN_HASH{$ENS_ID}="NONE"} 
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CNV_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if population CNV spans a gene 
                 if ($CNV_START < $GENE_START && $CNV_STOP > $GENE_STOP) {
                   if ($CNV_GAIN_HASH{$ENS_ID}eq"NONE") {$CNV_GAIN_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."S";} 
		   else {$CNV_GAIN_HASH{$ENS_ID}="$CNV_GAIN_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."S"; }}

		#If a populations CNV starts within a gene and ends outside
                 if ($GENE_START > $CNV_START && $GENE_START < $CNV_STOP && $GENE_STOP > $CNV_STOP) {
                   if ($CNV_GAIN_HASH{$ENS_ID}eq"NONE") {$CNV_GAIN_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."3P";}
                   else {$CNV_GAIN_HASH{$ENS_ID}="$CNV_GAIN_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."3P"; }}                

		#If a population CNV starts before a gene begins and ends inside
                 if ($GENE_START < $CNV_START && $GENE_STOP > $CNV_START &&  $GENE_STOP < $CNV_STOP) {
		   if ($CNV_GAIN_HASH{$ENS_ID}eq"NONE") {$CNV_GAIN_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."5P";}
                   else {$CNV_GAIN_HASH{$ENS_ID}="$CNV_GAIN_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."5P"; }}   

		#If a opoulationCNV is within a gene
                if ($CNV_START > $GENE_START && $CNV_STOP < $GENE_STOP) {
		   if ($CNV_GAIN_HASH{$ENS_ID}eq"NONE") {$CNV_GAIN_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."I";}
                   else {$CNV_GAIN_HASH{$ENS_ID}="$CNV_GAIN_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."I"; }} 

                }#Close for each
}


open CNV_LOSS, "/panfs/home/arobertson/DATA/WARD_DATA/supporting_variants_for_nstd46.csv.gz.copy_number_loss" or die "couldn't open the cnv_gain file: $!\n";

while (<CNV_LOSS>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        $CNV_CHR="$data[0]";
                if ($CNV_CHR eq 'Chr') {next;}
                if ($CNV_CHR eq 'X') {$CNV_CHR=23;}
                if ($CNV_CHR eq 'Y') {$CNV_CHR=24;}
        $CNV_START="$data[1]";
        $CNV_STOP="$data[2]";
        $CNV_ID="chr$CNV_CHR:$CNV_START-$CNV_STOP";
        $CNV_FREQ="$data[87]";
        $CNV_FREQ=$CNV_FREQ / 2062;
        $CNV_FREQ=$CNV_FREQ*100;
        $CNV_FREQ = sprintf "%.2f", $CNV_FREQ;

foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CNV_LOSS_HASH{$ENS_ID}) {} else {$CNV_LOSS_HASH{$ENS_ID}="NONE"}
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CNV_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if population CNV spans a gene 
                 if ($CNV_START < $GENE_START && $CNV_STOP > $GENE_STOP) {
                   if ($CNV_LOSS_HASH{$ENS_ID}eq"NONE") {$CNV_LOSS_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."S";}
                   else {$CNV_LOSS_HASH{$ENS_ID}="$CNV_LOSS_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."S"; }}

                #If a populations CNV starts within a gene and ends outside
                 if ($GENE_START > $CNV_START && $GENE_START < $CNV_STOP && $GENE_STOP > $CNV_STOP) {
                   if ($CNV_LOSS_HASH{$ENS_ID}eq"NONE") {$CNV_LOSS_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."3P";}
                   else {$CNV_LOSS_HASH{$ENS_ID}="$CNV_LOSS_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."3P"; }}

                #If a population CNV starts before a gene begins and ends inside
                 if ($GENE_START < $CNV_START && $GENE_STOP > $CNV_START &&  $GENE_STOP < $CNV_STOP) {
                   if ($CNV_LOSS_HASH{$ENS_ID}eq"NONE") {$CNV_LOSS_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."5P";}
                   else {$CNV_LOSS_HASH{$ENS_ID}="$CNV_LOSS_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."5P"; }}

                #If a opoulationCNV is within a gene
                if ($CNV_START > $GENE_START && $CNV_STOP < $GENE_STOP) {
                   if ($CNV_LOSS_HASH{$ENS_ID}eq"NONE") {$CNV_LOSS_HASH{$ENS_ID}="$CNV_ID"."_"."$CNV_FREQ"."_"."I";}
                   else {$CNV_LOSS_HASH{$ENS_ID}="$CNV_LOSS_HASH{$ENS_ID}"."--$CNV_ID"."_"."$CNV_FREQ"."_"."I"; }}

                }#Close for each
}


#	foreach $X (sort keys %CNV_LOSS_HASH) {
#	print "$X\t$CNV_LOSS_HASH{$X} \n";
#}


#--------------------
# Part two
#--------------------

open CONTROL1, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_065x1084_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control1 file: $!\n";

while (<CONTROL1>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
	if ($data[0] =~ /#/) {next;}
	$CON_CHR="$data[1]";
		if ($CON_CHR eq 'Chr') {next;}
	$CON_START="$data[2]";
	$CON_STOP="$data[3]";

	#print "$CON_CHR \n";	


	foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON1_HASH{$ENS_ID}) {} else {$CON1_HASH{$ENS_ID}="NO"} 
		$GENE_CHR=$GL_CHR{$ENS_ID};
		$GENE_START=$GL_START{$ENS_ID};
		$GENE_STOP=$GL_STOP{$ENS_ID};
		if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; }

		 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
		  $CON1_HASH{$ENS_ID}="YES"; } 	
		 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
		 $CON1_HASH{$ENS_ID}="YES"; }

		if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
		$CON1_HASH{$ENS_ID}="YES"; } 

		}#Close for each
}

#--------------------------------------------------------------------

#-----------------------------------------------------------------------
open CONTROL2, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_1084x065_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL2>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON2_HASH{$ENS_ID}) {} else {$CON2_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON2_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON2_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON2_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

#CON3
open CONTROL3, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_065x098_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL3>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
         if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON3_HASH{$ENS_ID}) {} else {$CON3_HASH{$ENS_ID}="NO"}
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON3_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON3_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON3_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON3_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

#CON4
open CONTROL4, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_098x065_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL4>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
         if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON4_HASH{$ENS_ID}) {} else {$CON4_HASH{$ENS_ID}="NO"}
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON4_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON4_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON4_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON4_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

#CON5
open CONTROL5, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_1084x098_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL5>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
         if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON5_HASH{$ENS_ID}) {} else {$CON5_HASH{$ENS_ID}="NO"}
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON5_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON5_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON5_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON5_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

#CON6

open CONTROL6, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/NEW_098x1084_i_19_i_[null]_i_[null]_e_all.txt_edited_CNV.txt" or die "couldn't open the control2 file: $!\n";

while (<CONTROL6>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
         if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";

        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                if (exists $CON6_HASH{$ENS_ID}) {} else {$CON6_HASH{$ENS_ID}="NO"}
                $GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $CON6_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $CON6_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $CON6_HASH{$ENS_ID}="YES"; }

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $CON6_HASH{$ENS_ID}="YES"; }

                }#Close for each
}

#########################################
open V098, "/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$alt_con" or die "couldn't open the $ward_name vs 098 file: $!\n";

while (<V098>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
        if ($data[0] =~ /#/) {next;}
        $CON_CHR="$data[1]";
	 if ($CON_CHR eq 'Chr') {next;}
        $CON_START="$data[2]";
        $CON_STOP="$data[3]";
#VS098_HASH
        foreach my $ENS_ID (sort keys %GL_HASH) { #for each CNV location
                 if (exists $VS098_HASH{$ENS_ID}) {} else {$VS098_HASH{$ENS_ID}="NO"}
		$GENE_CHR=$GL_CHR{$ENS_ID};
                $GENE_START=$GL_START{$ENS_ID};
                $GENE_STOP=$GL_STOP{$ENS_ID};
                if ($CON_CHR == $GENE_CHR ) {} else {next;}

                 #MODE A - if gene is inside a CNV
                 if ($CON_START < $GENE_START && $CON_STOP > $GENE_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; }

                 if ($GENE_START > $CON_START && $GENE_START < $CON_STOP && $GENE_STOP > $CON_STOP) {
                  $VS098_HASH{$ENS_ID}="YES"; } 

                 if ($GENE_START < $CON_START && $GENE_STOP > $CON_START &&  $GENE_STOP < $CON_STOP) {
                 $VS098_HASH{$ENS_ID}="YES"; } 

                if ($CON_START > $GENE_START && $CON_STOP < $GENE_STOP) {
                $VS098_HASH{$ENS_ID}="YES"; } 

                }#Close for each
}

#---------------------------------------------------

open  OUT_FINAL, ">/panfs/home/arobertson/DATA/WARD_DATA/OUTPUT/$final_name";

print OUT_FINAL "Gene\tEnsembl-ID\tGene-Type\tStatus\tGene-Location\tExpanded-Location\tCNV-type\tNo.CNVs\tCNVs\tMMR-GENE\tPopulation_Gain\tPopulation_Loss\t065v1084\t1084v065\t065v098\t098v065\t1084v098\t098v1084\tSamplev098\n";

 foreach my $ENS_ID (sort keys %GL_HASH) {
	print OUT_FINAL "$GL_HASH{$ENS_ID}\t$CNV_GAIN_HASH{$ENS_ID}\t$CNV_LOSS_HASH{$ENS_ID}\t$CON1_HASH{$ENS_ID}\t$CON2_HASH{$ENS_ID}\t$CON3_HASH{$ENS_ID}\t$CON4_HASH{$ENS_ID}\t$CON5_HASH{$ENS_ID}\t$CON6_HASH{$ENS_ID}\t$VS098_HASH{$ENS_ID}\n";
} 

=cut

# END END END END END            




