#!/usr/bin/perl -w

@data = ();

$R_CHR = '';
$R_START = '';
$R_STOP = '';
$R_CHANGE = '';
$R_SOURCE = '';
$Rx = '';
$Ry = '';
%R_HASH = ();

open RESULTS, "/panfs/home/arobertson/sCNAHitSeq/out/HCC1143_X_M100vN2/analysis/HCC1143_X_M100vN2.1.10.0223230427_1_all_0.0gb_1.25gb/cnv/HCC1143_X_M100vN2_i_13_i_\[null\]_i_\[null\]_e_all.txt" or die "couldn't open the results file: $!\n";

open RCNAs, "/panfs/home/arobertson/SCRIPTS/scripts/RecurrentsCNAs.csv" or die "couldn't open the results file: $!\n";

while (<RCNAs>) {
        my $line = $_;
	chomp $line;
	@data = split /,/, $line;
        #print "$line \n";
	$R_CHR = $data[0];
        	#if ($R_CHR eq 'Chromosome') {next;} 
	$R_START = $data[1];
	$R_STOP = $data[2];
	$R_CHANGE = $data[3];
	$R_SOURCE = $data[4];
	
        $Rx = "$R_CHR"."\t"."$R_START"."\t"."$R_STOP"."\t"."$R_CHANGE";
        $Ry = "$R_CHR"."\t"."$R_START"."\t"."$R_STOP";
        $R_HASH{$Ry}=$Rx;
	}
#--------------------

@bata = '';
$C_CHR = '';
$C_START = '';
$C_STOP = '';
$C_CHANGE = '';

$Cx = '';
$Cy = '';
%C_HASH = ();


while (<RESULTS>) {
        my $line = $_;
        chomp $line;
        @bata = split /\s+/, $line;
        #print "$line \n";
        $C_CHR = $bata[1];
                if ($C_CHR eq "Chr") {next;} 
		if ($bata[0] =~ "##") {next;}
        $C_START = $bata[2];
        $C_STOP = $bata[3];
        $C_CHANGE = $bata[6];

	#print "$C_CHR"."\t"."$C_START"."\t"."$C_STOP"."\t"."$C_CHANGE \n";

        $Cx = "$C_CHR"."\t"."$C_START"."\t"."$C_STOP"."\t"."$C_CHANGE";
        $Cy = "$C_CHR"."\t"."$C_START"."\t"."$C_STOP";
        $C_HASH{$Cy}=$Cx;
        }

my @data_result = ();

#--------------------------------

foreach my $C (sort keys %C_HASH) {	#for each of the events in 
	$X = $C_HASH{$C};
	@data_result = split /\t/, $X;
	$results_chr = $data_result[0];
	$results_start = $data_result[1];
	$results_stop = $data_result[2];
	$results_change = $data_result[3];
	$chr_results = "chr"."$results_chr";

		foreach my $R (sort keys %R_HASH) {
			$Y = $R_HASH{$R}; 
			@data_resource = split /\t/, $Y;
			        $resource_chr = $data_resource[0];
        			$resource_start = $data_resource[1];
        			$resource_stop = $data_resource[2];
       				$resource_change = $data_resource[3];
				#$chr_resource = "chr"."$resource_chr";
				#print "$chr_results \t $resource_chr \n"

#----------------------------------------------------------
# need two hashes to end
# here's where the magic happended
#----------------------------------------------------------

				
if ($chr_results eq $resource_chr) {
 

 if ($results_start > $resource_start) {
    if ($results_start < $resource_stop) {	
      if ($results_stop < $resource_stop) { 
	if ((($resource_change lt 0.5) && ($results_change lt 2)) ||($resource_change gt 0.5) && ($results_change gt 2)) {
	print "CLASS_ONE\t$chr_results\t$resource_chr\tEVENT\t$resource_start\t$resource_stop\tsCNAHitSeq\t$results_start\t$results_stop\t$resource_change\t$results_change \n";
			} 
		}}
	}

  if ($results_start > $resource_start){
    if ( $results_start < $resource_stop) {
      if ($results_stop > $resource_stop) {
	if ((($resource_change lt 0.5) && ($results_change lt 2)) ||($resource_change gt 0.5) && ($results_change gt 2)) {
	print "CLASS_TWO\t$chr_results\t$resource_chr\tEVENT\t$resource_start\t$resource_stop\tsCNAHitSeq\t$results_start\t $results_stop \t$resource_change\t$results_change \n";
		}
	}}
}
  if ((($results_start < $resource_start) && ($results_stop > $resource_start)) && ($results_stop < $resource_stop)) { 
	if ((($resource_change lt 0.5) && ($results_change lt 2)) ||($resource_change gt 0.5) && ($results_change gt 2)) {	
	print "CLASS_THREE\t$chr_results\t$resource_chr\tEVENT\t$resource_start\t$resource_stop\tsCNAHitSeq\t$results_start\t $results_stop \t$resource_change\t$results_change\n";	
	} }

  if ((( $results_start < $resource_start) && ($resource_start < $results_stop)) && ($resource_stop < $results_stop)) {
	if ((($resource_change lt 0.5) && ($results_change lt 2)) ||($resource_change gt 0.5) && ($results_change gt 2)) {
	 print "CLASS_FOUR\t$chr_results\t$resource_chr\tEVENT\t$resource_start\t$resource_stop\tsCNAHitSeq\t$results_start\t $results_stop \t$resource_change\t$results_change\n";
	}
}
}
}}
##
#\	}

	
=cut
#CLASS THREE - 5' EXTERNAL, 3' INTERNAL
if ($results_stop ge $resource_start && $results_stop le $resource_stop) { 
print "CLASS THREE - 3'INTERNAL/ 5'EXTERNAL \n$chr_results event = $resource_start : $resource_stop - sCNAHitSeq: $results_start : $results_stop \n"; } 
		}

	}
}



 else {
#CLASS FOUR - sCNA engulfs entire recurrent event
if ($resource_start ge $results_start && $resource_start le $results_stop) {
	if ($resource_stop le $results_stop) {
print "CLASS FOUR - sCNA spans entire Recurrent event \n$chr_results event = $resource_start : $resource_stop - sCNAHitSeq: $results_start : $results_stop \n";}				}
			}
		}
	}
	}
