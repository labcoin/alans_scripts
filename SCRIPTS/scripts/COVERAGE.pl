#!/usr/bin/perl -w
use strict;
#---------------------------------

my $NAME = 'TEST';

my @data = ();		#array containing raw data
my $genome = '';	#if then genome then proceed, else next
my $depth = '';		#stores the depth of coverafe
my $bases = '';		#variable used to combine the counts from SampleA and SampleB
my $totalbases = '';	#variable defining format
my $cume = 0;
my $times = 0;
my $cover = '0';

open INPUT, "/panfs/home/arobertson/HiSeqX/out/COVERAGE/APGI_2057_WGS_40_coverage.txt" or die "couldn't open the results file: $!\n";

#open OUT, ">/panfs/home/arobertson/${NAME}_X.txt";

while (<INPUT>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;
	$genome = $data[0];
	if ($genome eq 'genome')	#remove qCNV header and replace with qBAMheader
		{	$depth = $data[1];
			$bases = $data[2];
			$totalbases = $data[3];
			$times = $depth*$bases;
			$cume = $times + $cume;
			if ($cume > 1) {
				$cover = $cume / $totalbases; }
		} else {next;}
	}
print "$cover \n";
