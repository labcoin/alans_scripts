#!/usr/bin/perl -w

#this script has been written to determine the copy number state of regions specifically associated with different types of cancer

#it requires a reference file, a file containing all the peaks of mutation associated with the different types of cancer, the one used here is from the TCGA pan cancer analysis

#it then opens the output file from sCNAHitSeq, preprocesses the data and then looks for overlap

#-------------------
# Part One - LOAD REFERENCE FILE INTO HASH
#--------------------

open REFERENCE, "/panfs/home/arobertson/SCRIPTS/scripts/CancerType_sCNAs_Matrix.txt" or die "couldn't open the refernece file: $!\n";

#The reference file contains information in the following format
#CANCER_TYPE	LOCATION	DIRECTION

#Store information in a hash as:
# UCEC.chr2:1-27588658 = UCEC\tchr2:1-27588658\t3 

my $REF_1 = '';
my $REF_2 = '';
my $REF_3 = '';
my $ID = '';
my %REF_HASH = ();

while (<REFERENCE>) {
        my $line = $_;
        chomp $line;
        @data = split  /\s+/, $line;
        #print "$line \n";
	$REF_1 = $data[0];
                if ($REF_1 eq "Cancer") {next;}
	$REF_2 = $data[1];
	$REF_3 = $data[2];
	$ID = "$REF_1.$REF_2";
	#print "ID = $ID \n";
#Store this information in a hash
	$REF_HASH{$ID}=$line;
}

print "Recurrent Mutational Peaks Imported! \n";
		
#--------------------------
# PART TWO - PREPROCESS SCNAHITSEQ OUTPUT
# put into same format
#-------------------------
open  OUT, ">/panfs/home/arobertson/SCRIPTS/scripts/HCC1143_M05.txt";
print OUT "ID\tOverlap_Class\tCancer_Type\tPeak_Direction\t_Peak_Chr\tPeak_Start\tPeak_Stop\tsCNA_Direction\tsCNA_Chr\tsCNA_Start\tsCNA_Stop\n";

open RESULTS, "/panfs/home/arobertson/SCRIPTS/scripts/TIME_HCC1143_M05_i_23_i_[null]_i_[null]_e_all.txt_edited_CNV-2.txt" or die "couldn't open the results file: $!\n";

while (<RESULTS>) {
        my $line = $_;
        chomp $line;
        @bata = split /\s+/, $line;
		if ($bata[0] eq "Sample" ) {next;}
		if ($bata[0] =~ "##" ) {next;}
		if ($bata[1] == "23" ) {$bata[1]="X";}
	$RESULTS_CHR = "chr$bata[1]";
	$RESULTS_START = $bata[2];
	$RESULTS_STOP = $bata[3];
	$RESULTS_DIRECTION = $bata[6];
		if ($RESULTS_DIRECTION > 3) {$RESULTS_DIRECTION = 3;}
	#print "chr = $RESULTS_CHR \t start = $RESULTS_START \t stop = $RESULTS_STOP \t $RESULTS_DIRECTION \n";
	
#--------------------------
# PART THREE - OVERLAP
# for each of the peaks in the TCGA data
# if not on same chromososome skip
#-------------------------

		foreach my $X (sort keys %REF_HASH) {
		$Y = $REF_HASH{$X};
		@mata = split /\s+/, $Y;
		$TYPE=$mata[0];
		$LOC=$mata[1];
		$DIR=$mata[2];
			if ($RESULTS_DIRECTION == $DIR ) {} else {next;} #if same direction keep going, else next
		@nata = split /:/, $LOC;
		$CHR = $nata[0];
			if ($CHR eq $RESULTS_CHR ) {} else {next;} # if same chromosome keep going else next
		@wata = split /-/, $nata[1];
		$REF_START = $wata[0];
		$REF_STOP = $wata[1];	
		#%PRINT_HASH = ();
	
		#MODE A - if sCNA is inside REF peak
		if ($REF_START < $RESULTS_START && $REF_STOP > $RESULTS_STOP) {
			print OUT "$X\tCLASS_A\t$TYPE\t$DIR\t$CHR\t$REF_START\t$REF_STOP\t$RESULTS_DIRECTION\t$RESULTS_CHR\t$RESULTS_START\t$RESULTS_STOP\n";}

		 #MODE B - sCNA starts inside but finishes outside
		if ($RESULTS_START > $REF_START && $RESULTS_START < $REF_STOP && $RESULTS_STOP > $REF_STOP) {
			print OUT "$X\tCLASS_B\t$TYPE\t$DIR\t$CHR\t$REF_START\t$REF_STOP\t$RESULTS_DIRECTION\t$RESULTS_CHR\t$RESULTS_START\t$RESULTS_STOP\n"; } 
		
               #MODE c - sCNA starts before ref but finishes inside
                if ($RESULTS_START < $REF_START && $RESULTS_STOP > $REF_START &&  $RESULTS_STOP < $REF_STOP ) {
                        print OUT  "$X\tCLASS_C\t$TYPE\t$DIR\t$CHR\t$REF_START\t$REF_STOP\t$RESULTS_DIRECTION\t$RESULTS_CHR\t$RESULTS_START\t$RESULTS_STOP\n";}
	
		#Mode D - if mutation engulfs REF peak
	 if ($REF_START > $RESULTS_START && $REF_STOP < $RESULTS_STOP) {
                        print OUT "$X\tCLASS_D\t$TYPE\t$DIR\t$CHR\t$REF_START\t$REF_STOP\t$RESULTS_DIRECTION\t$RESULTS_CHR\t$RESULTS_START\t$RESULTS_STOP\n";} 
	} #close for loop

} #close while

#----------------------------
#PART FOUR
#---------------------------



