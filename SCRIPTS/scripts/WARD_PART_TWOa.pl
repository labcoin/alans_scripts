#!/usr/bin/perl -w

#----------
# This script has been written to turn the output from the first pass of the vsqr.vep.vcf into something more human readable 
#----------

# Things to do
# 1.A skip VQSRTrancheSNP99.90to100.00 - false positivies
# 1.B Strip everything back to 0/0, 2/3, etc
# 1.C number of samples that contain SNP

# 2. Create Gene centric output
#	i.e. MLH1 ENSG0000012312 TotalSNPs NovelSNPs 
#

open DATA, "/panfs/home/arobertson/SCRIPTS/scripts/SECOND_PASS.txt" or die "couldn't open the GENELIST file: $!\n";

@test=();
%ID_HASH=();
%SUM_HASH=();
%DATA_HASH=();

while (<DATA>) {
        $line = $_;
        chomp $line;
        @data = split  /\t/, $line;
	$ID="$data[0]"."_"."$data[1]";
	if ($data[8] =~ 'VQSRTrancheSNP99.90to100.00') {next;}	#Remove false positives
	if ($data[0] eq 'SNP-ID') {next;}			#Remove header
	if ($data[4] =~ /\./) {
	#print "$data[2]\t$data[4]\t";
	@array = split  /_/, $data[0];
	$loc="chr"."$array[0]".":"."$array[1]";
	$ID_HASH{$ID}="$data[2]\t$data[1]\t$loc\t$data[5]\t$data[6]";
	$germline_count=0;
	$undetermined_count="0";
	$SNP_count=13;
	$var0=10;
	
	while ($var0 < 23 ) {
		@test = split  /:/, $data[$var0];
		if (exists $DATA_HASH{$ID}) {$DATA_HASH{$ID} = "$DATA_HASH{$ID}\t\'$test[0]";} else {$DATA_HASH{$ID}="\'$test[0]"}

		#print "$test[0]\t";
			if ($test[0] =~ /0\/0/) {$germline_count++;}
			if ($test[0] =~ /\.\/\./) {$undetermined_count++;}		
			$var0++;

			if ($var0 == 23) {	$SNP_count=$SNP_count - $germline_count; 
						$SNP_count=$SNP_count - $undetermined_count;  
						$SUM_HASH{$ID}="$SNP_count\t$germline_count\t$undetermined_count";}
			}
		}
	}

print "Gene\tENSG-ID\tSNP-Location\tREF\tALT\tNo.SNPs\tNo.WtSNPs\tNo.Undetermined\t17_2098\tG252\tG272\tG273\tG278\tG302\tG309\tG339\tG340\tG348\tG325\t17_2003\tG365\n"; 

foreach $Z (sort keys %ID_HASH) {
	print "$ID_HASH{$Z}\t$SUM_HASH{$Z}\t$DATA_HASH{$Z}\n";
	}
	
	

