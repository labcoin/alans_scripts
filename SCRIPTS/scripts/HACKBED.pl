#!/usr/bin/perl -w
use strict;
#---------------------------------
# PreProcessQBam
# I want to generate a dirty .bed file using the start and end positions from the qCNV output files 
#--------------------------------
=cut

Desired output

#chrom	start	end	paternal_copy maternal_copy
chr1	0	999	1	1	

qCNV output
#CHROM	ID	START	END	FORMAT	SAMPLEA	SAMPLEB
chr1	1_1000	1	999	DP	414	84


=cut

my @data = ();		#array containing raw data
my $sampleA = '';	#stores counts from SampleA
my $sampleB = '';	#stores counts from SampleB
my $counts = '';	#variable used to combine the counts from SampleA and SampleB
my $format = '';	#variable defining format

open (INPUT,"/panfs/home/arobertson/OESO/counts/TRANSFORMED/OESO_1527.counts") or die "couldn't open the results file: $!\n";

open  OUT, ">/panfs/home/arobertson/hackedbed.bed";

while (<INPUT>) {
        my $line = $_;
        chomp $line;
        @data = split /\t/, $line;

	if ($data[0] eq '#CHROM')					#remove qCNV header and replace with qBAMheader
		{print OUT "#Chrom\tStart\tEnd\tFormat\tCounts\n";
		next;}

	$format = $data[4];
	if ($format eq 'DP') {
		$sampleA = $data[6];
		$sampleB = $data[5];
		$counts = $sampleA.":".$sampleB;
		print OUT "$data[0]\t$data[2]\t$data[3]\tP0:Ref\t$counts\n";
	} else {
		$format = 'END';
                $sampleA = $data[5];
                $sampleB = $data[4];
                $counts = $sampleA.":".$sampleB; 
                print OUT "$data[0]\t$data[2]\t$data[3]\tP0:Ref\t$counts\n";
		}
	}

print "Reformating complete \n";
