#!/usr/bin/perl -w
use strict;
#---------------------------------
# Convert sCNA changes  
# This script has been written to convert the new way of recording sCNA changes in 0.7 (1 = 0.5, 2=1, 3=1.5, etc), into a format that can be compared wothe array data
#--------------------------------
=cut

Current output
Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
EHLC_FETAL01vM4        8 43000001 46800000      10  3.80e+06 0.500  1.00
EHLC_FETAL01vM4       15 20207003 22507001      20  2.30e+06  2.00  1.00

EHLC_FETAL01vM4	8	43000001	46800000	10	7600000	0.500
EHLC_FETAL01vM4	15	20207003	22507001	20	4600000	2.00

Transform
1. Remove ID col
2. Combine SampleA and SampleB cols
3. Replace format?

=cut

my @data = ();		#array containing raw data
my $sampleA = '';	#stores counts from SampleA
my $sampleB = '';	#stores counts from SampleB
my $counts = '';	#variable used to combine the counts from SampleA and SampleB
my $format = '';	#variable defining format
my $chr = '';

my $loc="/panfs/home/arobertson/7_HitSeq/out/PAPER_2057_M101/analysis/PAPER_2057_M101.2.10.0203131548_1_all_1_0mb_X_300mb/cnv/";
my $name="PAPER_2057_M101_i_22_i_[null]_i_[null]_e_all.txt";
my $new_name="edited_CNV.txt";

open (INPUT,"$loc$name") or die "couldn't open the results file: $!\n";

open  OUT,">$loc$new_name";


while (<INPUT>) {
        my $line = $_;
        chomp $line;
        @data = split /\s+/, $line;

	if ($data[0] eq 'Sample')					#remove qCNV header and replace with qBAMheader
		{	print OUT "Sample Chr FirstProbe LastProbe NbSnp length_min Type Avg_certainty\n";
			#print OUT "Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty\n";
			next;}

	if ($data[0] =~/##/) {print OUT "$line\n"; next;}
	#convert X to 23
	$chr = $data[1];
	 if ($chr eq 'X') {$chr = '23';}
		

	#double CNV
	$format = $data[6];
	if ($format = 1.0) {
		#$sampleA = $data[6];
		$sampleB = $data[6] + $data[6];
		#$counts = $sampleA.":".$sampleB;
		print OUT "$data[0]\t$chr\t$data[2]\t$data[3]\t$data[4]\t$data[5]\t$sampleB\t$data[7]\n";
			}
}
	

print "Reformating complete \n";
