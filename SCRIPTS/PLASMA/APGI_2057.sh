LIB=/panfs/home/arobertson/PlasmaCNV_0.3/lib
java=/panfs/home/arobertson/CNV/jre1.6.0_19/bin/java
#java=/usr/bin/java
#sampleid=$1
sampleid=APGI_2057
#cumR=$2
cumR=10
#baseDir="../data/"  ##location of different files with zip directories
baseDir="/panfs/home/arobertson/CNV/TEST/APGI_2057/data/"

#baseDir="/home/lcoin/group_coin/Users/s.song/Share/p48_20131218_plasmaCNV_analysis/"
#baseDir="/home/lcoin/group_coin/Users/s.song/Share/qCNV/4esophageal/"

plot=1
# 0 = no plot
# 1 = file
# 2 = on screen
initialC=0.99

ploidy=2
copies="0:1:2:3:4:5:6"
color="pink:red~0.5:gray~0.5:green:cyan:blue:magenta:orange:yellow:green~0.3:cyan~0.3:blue~0.3:magenta~0.3:orange~0.3:yellow~0.3"

#ploidy=3
#copies=0:1:2:3   
#color="pink:red~0.5:red:gray~0.5:green:cyan:blue:magenta:orange:yellow:green~0.3:cyan~0.3:blue~0.3:magenta~0.3:orange~0.3:yellow~0.3"

#echo $color

mem=46900m

dat=$(date +%m%d%H%M%S)



ls $LIB>lib_list_$dat
while read line; do
class=$class:"$LIB/$line"
done < lib_list_$dat
rm lib_list_$dat




echo $class


line="$java  -Xmx$mem -server -cp $class lc1.dp.appl.CNVHap --paramFile  param.txt --index 1 --column 1 --baseDir  $baseDir  --numIt 10  --plot $plot --showScatter true  --r_panel true  --b_panel true  --showHMM false    --showHaps false  --restrictKb 0kb:0kb  --experiment $sampleid.$cumR.$dat  --include $sampleid   --initialCellularity 0.99:1.0  --downsample 1:1  --dilute 1:0   --cumulativeR $cumR --useAvgDepth  true   --toInclude  all  --makeNewRef false  --maxPloidy1 1 --noCopies $ploidy --backgroundCount $ploidy --modify0 $copies --color $color --ratioAsLevels null  --trainCellularity 2 --reference NORMAL"

$line


