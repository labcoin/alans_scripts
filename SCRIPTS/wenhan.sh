#!/bin/bash - 
#===============================================================================
#
#           FILE: script.sh
#    DESCRIPTION: Separate chrom, make sorted bam and vcf
#          NOTES: ---
#         AUTHOR: Wenhan Chen (Wenhan), 
#        CREATED: 02/05/14 15:06
# Last_Modified : 03 May 2014 21:11:55
#===============================================================================


export REQUIRED_RESOURCES=1:2:12GB:46:normal


# step 0
set -o nounset                              # Treat unset variables as an error
set -e


#for xx in {{1..22},X,Y}; do
xx=6
#for xx in {2,X}; do
echo $xx


#prefix=G15511.HCC1143_BL.1
prefix=G15511.HCC1143.1
#prefix=HCC1143.NORMAL.30x.compare
#prefix=HCC1143.mix1.n80t20
#prefix=HCC1143.mix1.n95t5
#prefix=HCC1143.mix1.n40t60
#prefix=HCC1143.mix1.n60t40
#prefix=Galaxy_Plasma_DNA_PCR_duplicates_removed
#prefix=HCC1143.mix1.n5t95
#prefix=HCC1143.mix1.n20t80

jumpOver="n"

script=`cat << EOF
#!/bin/bash
#PBS -N tumor_p
    set -o nounset                              # Treat unset variables as an error
    set -e


    echo " ----------------  seting env  ---------------------------------"
    cd $WORK/tmp
    export LD_LIBRARY_PATH=.:${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
    export PATH=$UTILS:$PATH

if [[ $jumpOver == n ]] ; then
    echo "step 1:"
    samtools view -q 2 -bh $prefix.bam $xx > $prefix.chr$xx.bam 

    echo "step 2:"
    samtools sort $prefix.chr$xx.bam $prefix.sorted.chr$xx

    echo "step 3:"
    samtools index  $prefix.sorted.chr$xx.bam
fi
    echo "step 4:"
    # samtools mpileup -uf chr$xx.fa $prefix.sorted.chr$xx.bam |  bcftools view -vcg - > $prefix.sorted.chr$xx.vcf
    #cat G15511.HCC1143_BL.1.sorted.chr$xx.vcf | sed -e '/^#/d' -e '/INDEL/d' | cut -f1,2 | uniq > normal.chr$xx.heteroloci
    samtools mpileup -q 2 -uf chr$xx.fa $prefix.sorted.chr$xx.bam  | bcftools view -l normal.chr$xx.heteroloci -cg - > $prefix.sorted.chr$xx.vcf


# For running dfextract which prepares input for climat. 
# Note: mpileup should preform without -u.

    #samtools mpileup -q 2 -f chr$xx.fa $prefix.sorted.chr$xx.bam  > $prefix.sorted.chr$xx.pileup
   #./DFExtract -w 1000 -Q 10 -q 20 -p 33 -d 5 -s hg19_snp138_1based.txt -r chr$xx.fa -m map.bw  $prefix.sorted.chr$xx.pileup > $prefix.sorted.chr$xx.alleleDepth.dfe



    echo "done"
EOF`




#a_script=`echo "$script" | sed "s/\$xx/$xx/"`
printf "%s\n" "$script" > tmp.sh
qsub  tmp.sh
#qsub -a 131900 tmp.sh
#done



#bash  <(printf "%s" "$script")


