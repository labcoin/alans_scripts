Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
HT29_05vF1_2_NoGC       18 76624002 77624004       2  1.00e+06  1.50  1.00
HT29_05vF1_2_NoGC       19  545999 18546000      19  1.80e+07  1.50  1.00
HT29_05vF1_2_NoGC       19 33546000 55546001      23  2.20e+07  1.50  1.00
##Estimated cellularity:1.0
##Estimated ratio:1.0074730622175878
##Version:2.0
