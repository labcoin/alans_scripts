Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
HT29_20vF1_3_NoGC       11 61086000 76085999      16  1.50e+07  1.33  1.00
HT29_20vF1_3_NoGC       12 131079000 133079004       3  2.00e+06  1.33  1.00
HT29_20vF1_3_NoGC       13 18227004 36227003      19  1.80e+07  1.33  1.00
HT29_20vF1_3_NoGC       19 28546001 58546002      31  3.00e+07  1.33  1.00
HT29_20vF1_3_NoGC       20  417000  417000       1   0.00  1.33  1.00
HT29_20vF1_3_NoGC       20 30416999 62417003      33  3.20e+07  1.33  1.00
HT29_20vF1_3_NoGC        8 115200000 145200000      31  3.00e+07  1.67  1.00
##Estimated cellularity:0.916
##Estimated ratio:1.019001274475732
##Version:2.0
