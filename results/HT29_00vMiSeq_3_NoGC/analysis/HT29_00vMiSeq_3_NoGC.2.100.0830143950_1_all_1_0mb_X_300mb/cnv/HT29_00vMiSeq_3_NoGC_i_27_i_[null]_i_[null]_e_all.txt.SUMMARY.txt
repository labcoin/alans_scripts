Experimental ID:		HT29_00vMiSeq_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_00/EHLC_HT29_00_FSD.bam 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.68
##Estimated ratio:0.91
##Version:2.0
Max. possible CNVs:		31371.61
Number of CNVs:			106
Total length of CNVs:		1886500016 bp
Average Length of CNV:		17797169.9622642 bp
WARNING:			NONE
