Experimental ID:		HT29_20vF1_2_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_20/EHLC_HT29_20_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.84
##Estimated ratio:1.0191171359054572
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			5
Total length of CNVs:		105000006 bp
Average Length of CNV:		21000001.2 bp
WARNING:			NONE
