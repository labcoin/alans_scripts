Experimental ID:		HT29_100vF5_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F5/EHLC_F5_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			250 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.0371763470958404
##Version:2.0
Max. possible CNVs:		12548.644
Number of CNVs:			46
Total length of CNVs:		1208999985 bp
Average Length of CNV:		26282608.3695652 bp
WARNING:			NONE
