Experimental ID:		HT29_100vF7_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F7/EHLC_F7_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			250 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.0689816029621648
##Version:2.0
Max. possible CNVs:		12548.644
Number of CNVs:			65
Total length of CNVs:		1319750017 bp
Average Length of CNV:		20303846.4153846 bp
WARNING:			NONE
