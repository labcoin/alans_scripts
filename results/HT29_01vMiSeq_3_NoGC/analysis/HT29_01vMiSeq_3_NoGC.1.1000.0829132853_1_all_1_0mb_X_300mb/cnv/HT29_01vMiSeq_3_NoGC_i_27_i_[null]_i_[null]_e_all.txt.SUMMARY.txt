Experimental ID:		HT29_01vMiSeq_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_01/EHLC_HT29_01_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.69
##Estimated ratio:0.916348047734911
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			102
Total length of CNVs:		1770999990 bp
Average Length of CNV:		17362745 bp
WARNING:			NONE
