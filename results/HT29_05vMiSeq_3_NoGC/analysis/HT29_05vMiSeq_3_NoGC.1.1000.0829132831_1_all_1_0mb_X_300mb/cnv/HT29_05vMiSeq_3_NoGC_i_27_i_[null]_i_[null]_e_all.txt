Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
HT29_05vMiSeq_3_NoGC        1 186000000 199000002      14  1.30e+07 0.333  1.00
HT29_05vMiSeq_3_NoGC       13 54227004 71227001      18  1.70e+07 0.333  1.00
HT29_05vMiSeq_3_NoGC        1 49000002 121000002      73  7.20e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        2 31749000 88749000      58  5.70e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        2 114749004 215749002     102  1.01e+08 0.667  1.00
HT29_05vMiSeq_3_NoGC        3 16549002 35549003      20  1.90e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        3 57548999 117549000      59  6.00e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        3 141549000 192549000      52  5.10e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        4 10526004 190526003     179  1.80e+08 0.667  1.00
HT29_05vMiSeq_3_NoGC        5  371003 130370999     129  1.30e+08 0.667  1.00
HT29_05vMiSeq_3_NoGC        6 46454999 166455000     119  1.20e+08 0.667  1.00
HT29_05vMiSeq_3_NoGC        7 7339001 21339000      15  1.40e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        7 76339002 125339004      50  4.90e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        8 13200000 116200001     102  1.03e+08 0.667  1.00
HT29_05vMiSeq_3_NoGC        9  835002 31835003      32  3.10e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       10 42621000 69621000      28  2.70e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       11 21086004 50086001      30  2.90e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       11 80086002 109085999      30  2.90e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       12 14078999 46079003      31  3.20e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       12 59079000 100079003      42  4.10e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       13 72227004 108227003      37  3.60e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       14 25057001 90057000      66  6.50e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       18 18624000 70624002      53  5.20e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC       21 13391003 31391003      19  1.80e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        X 3956004 37955999      35  3.40e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        X 76955999 145955999      70  6.90e+07 0.667  1.00
HT29_05vMiSeq_3_NoGC        1       0 47999999      49  4.80e+07  1.33  1.00
HT29_05vMiSeq_3_NoGC        2 89749002 91748999       3  2.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC        3 193549001 197549003       5  4.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC        4  526001 9526001      10  9.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC        6 167455002 170455002       4  3.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC        7  338999 6339000       7  6.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC        8 117200004 146200001      30  2.90e+07  1.33  1.00
HT29_05vMiSeq_3_NoGC       19 28546001 58546002      31  3.00e+07  1.33  1.00
HT29_05vMiSeq_3_NoGC       20 29416998 62417003      34  3.30e+07  1.33  1.00
HT29_05vMiSeq_3_NoGC       21 9391002 10391003       2  1.00e+06  1.33  1.00
HT29_05vMiSeq_3_NoGC       10 38621003 41621003       2  3.00e+06  1.67  1.00
##Estimated cellularity:0.459
##Estimated ratio:0.8488008342022944
##Version:2.0
