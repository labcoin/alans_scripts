Experimental ID:		HT29_100vF1_2_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.707
##Estimated ratio:1.0170316301703164
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			42
Total length of CNVs:		1223000023 bp
Average Length of CNV:		29119048.1666667 bp
WARNING:			NONE
