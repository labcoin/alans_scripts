Experimental ID:		HT29_100vF1_2_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			250 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.697
##Estimated ratio:1.0263733379986004
##Version:2.0
Max. possible CNVs:		12548.644
Number of CNVs:			47
Total length of CNVs:		1270000026 bp
Average Length of CNV:		27021277.1489362 bp
WARNING:			NONE
