Experimental ID:		HT29_50vF1_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_50/EHLC_HT29_50_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.676
##Estimated ratio:1.030239833159532
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			31
Total length of CNVs:		1183000026 bp
Average Length of CNV:		38161291.1612903 bp
WARNING:			NONE
