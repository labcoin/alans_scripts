Experimental ID:		HT29_100vF2_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F2/EHLC_F2_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.0369597960838701
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			33
Total length of CNVs:		1293000019 bp
Average Length of CNV:		39181818.7575758 bp
WARNING:			NONE
