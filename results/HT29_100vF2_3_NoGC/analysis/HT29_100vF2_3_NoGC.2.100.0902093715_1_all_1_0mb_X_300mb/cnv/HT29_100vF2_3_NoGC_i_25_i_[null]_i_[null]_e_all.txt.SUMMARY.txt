Experimental ID:		HT29_100vF2_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F2/EHLC_F2_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.972
##Estimated ratio:1.043
##Version:2.0
Max. possible CNVs:		31371.61
Number of CNVs:			40
Total length of CNVs:		1378699994 bp
Average Length of CNV:		34467499.85 bp
WARNING:			NONE
