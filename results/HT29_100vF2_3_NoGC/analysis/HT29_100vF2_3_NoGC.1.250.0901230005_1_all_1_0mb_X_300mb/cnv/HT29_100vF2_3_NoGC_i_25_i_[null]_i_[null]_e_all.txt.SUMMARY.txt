Experimental ID:		HT29_100vF2_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F2/EHLC_F2_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			250 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.972
##Estimated ratio:1.0438205195486276
##Version:2.0
Max. possible CNVs:		12548.644
Number of CNVs:			36
Total length of CNVs:		1357250026 bp
Average Length of CNV:		37701389.6111111 bp
WARNING:			NONE
