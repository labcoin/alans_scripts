Experimental ID:		HT29_50vMiSeq_2_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_50/EHLC_HT29_50_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.507
##Estimated ratio:1.0050399721932568
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			34
Total length of CNVs:		964000027 bp
Average Length of CNV:		28352941.9705882 bp
WARNING:			NONE
