Experimental ID:		HT29_100vMiSeq_2_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/MiSeq_Normal_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.753
##Estimated ratio:0.9935696906499826
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			51
Total length of CNVs:		1035000036 bp
Average Length of CNV:		20294118.3529412 bp
WARNING:			NONE
