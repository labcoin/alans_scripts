Experimental ID:		HT29_100vF1_2_GC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			TRUE 
##Estimated cellularity:0.712
##Estimated ratio:1.043
##Version:2.0
Max. possible CNVs:		31371.61
Number of CNVs:			55
Total length of CNVs:		1299500008 bp
Average Length of CNV:		23627272.8727273 bp
WARNING:			NONE
