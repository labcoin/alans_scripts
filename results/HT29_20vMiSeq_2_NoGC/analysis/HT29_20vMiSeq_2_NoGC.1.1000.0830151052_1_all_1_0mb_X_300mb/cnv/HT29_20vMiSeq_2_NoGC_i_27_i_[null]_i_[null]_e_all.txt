Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
HT29_20vMiSeq_2_NoGC        3 76549002 89549003      14  1.30e+07 0.500  1.00
HT29_20vMiSeq_2_NoGC       10 38621003 41621003       2  3.00e+06  1.50  1.00
HT29_20vMiSeq_2_NoGC       11 51086004 76085999      24  2.50e+07  1.50  1.00
HT29_20vMiSeq_2_NoGC       12 131079000 133079004       3  2.00e+06  1.50  1.00
HT29_20vMiSeq_2_NoGC       13 18227004 36227003      19  1.80e+07  1.50  1.00
HT29_20vMiSeq_2_NoGC       17 28820003 37820004      10  9.00e+06  1.50  1.00
HT29_20vMiSeq_2_NoGC       19 28546001 58546002      31  3.00e+07  1.50  1.00
HT29_20vMiSeq_2_NoGC       20  417000  417000       1   0.00  1.50  1.00
HT29_20vMiSeq_2_NoGC       20 24416999 62417003      36  3.80e+07  1.50  1.00
HT29_20vMiSeq_2_NoGC        8 115200000 143200001      29  2.80e+07  2.00  1.00
HT29_20vMiSeq_2_NoGC       10 113621003 114621000       2  1.00e+06  2.00  1.00
##Estimated cellularity:0.632
##Estimated ratio:1.0304136253041363
##Version:2.0
