Experimental ID:		HT29_05vF1_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_05/EHLC_HT29_05_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.669
##Estimated ratio:1.0601320820299114
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			16
Total length of CNVs:		503000014 bp
Average Length of CNV:		31437500.875 bp
WARNING:			NONE
