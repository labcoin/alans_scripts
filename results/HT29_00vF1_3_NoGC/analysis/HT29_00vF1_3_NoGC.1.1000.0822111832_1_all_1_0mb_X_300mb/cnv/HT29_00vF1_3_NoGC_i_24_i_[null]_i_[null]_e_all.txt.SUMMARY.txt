Experimental ID:		HT29_00vF1_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_00/EHLC_HT29_00_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.66
##Estimated ratio:0.9124087591240887
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			63
Total length of CNVs:		1679000025 bp
Average Length of CNV:		26650794.047619 bp
WARNING:			NONE
