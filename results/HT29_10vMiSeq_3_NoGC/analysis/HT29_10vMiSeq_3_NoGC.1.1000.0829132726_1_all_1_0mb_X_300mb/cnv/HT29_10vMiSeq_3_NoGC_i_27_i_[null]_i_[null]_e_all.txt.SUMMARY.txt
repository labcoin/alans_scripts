Experimental ID:		HT29_10vMiSeq_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_10/EHLC_HT29_10_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.0198123044838394
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			11
Total length of CNVs:		196999990 bp
Average Length of CNV:		17909090 bp
WARNING:			NONE
