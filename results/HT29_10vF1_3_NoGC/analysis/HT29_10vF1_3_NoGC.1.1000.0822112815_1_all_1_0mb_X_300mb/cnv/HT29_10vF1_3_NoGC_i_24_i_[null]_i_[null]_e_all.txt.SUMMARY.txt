Experimental ID:		HT29_10vF1_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F1/EHLC_F1_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_10/EHLC_HT29_10_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	2 
GC Correction:			FALSE 
##Estimated cellularity:0.995
##Estimated ratio:1.0093847758081336
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			2
Total length of CNVs:		52999998 bp
Average Length of CNV:		26499999 bp
WARNING:			NONE
