Sample       Chr FirstProbe LastProbe   NbSnp  length_min    Type Avg_certainty
HT29_10vF1_3_NoGC        8 117200004 145200000      29  2.80e+07  1.50  1.00
HT29_10vF1_3_NoGC       19 33546000 58546002      26  2.50e+07  1.50  1.00
##Estimated cellularity:0.995
##Estimated ratio:1.0093847758081336
##Version:2.0
