Experimental ID:		HT29_50vMiSeq_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_50/EHLC_HT29_50_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:0.749
##Estimated ratio:1.0100799443865072
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			35
Total length of CNVs:		973000021 bp
Average Length of CNV:		27800000.6 bp
WARNING:			NONE
