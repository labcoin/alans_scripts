Experimental ID:		HT29_100vF6_3_NoGC 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
Estimated cellularity	1.0
Estimated ratio	1.045
Estimated 	
Max. possible CNVs:		31371.61
Number of CNVs:			52
Total length of CNVs:		1330299986 bp
Average Length of CNV:		25582692.0384615 bp
WARNING:			NONE
