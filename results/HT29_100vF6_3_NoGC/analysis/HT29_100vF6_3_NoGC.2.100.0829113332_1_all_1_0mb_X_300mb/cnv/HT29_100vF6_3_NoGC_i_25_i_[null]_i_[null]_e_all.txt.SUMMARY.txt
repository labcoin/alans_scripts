Experimental ID:		HT29_100vF6_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F6/EHLC_F6_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.031
##Version:2.0
Max. possible CNVs:		31371.61
Number of CNVs:			52
Total length of CNVs:		1323500008 bp
Average Length of CNV:		25451923.2307692 bp
WARNING:			NONE
