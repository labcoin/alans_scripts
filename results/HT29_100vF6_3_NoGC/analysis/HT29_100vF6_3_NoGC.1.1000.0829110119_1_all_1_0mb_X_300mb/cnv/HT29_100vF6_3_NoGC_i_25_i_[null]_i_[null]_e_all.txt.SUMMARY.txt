Experimental ID:		HT29_100vF6_3_NoGC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_F6/EHLC_F6_FSD.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				1 
Location:			1_0mb_X_300mb 
Window Size:			1000 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			FALSE 
##Estimated cellularity:1.0
##Estimated ratio:1.0318618931757513
##Version:2.0
Max. possible CNVs:		3137.161
Number of CNVs:			41
Total length of CNVs:		1254000035 bp
Average Length of CNV:		30585366.7073171 bp
WARNING:			NONE
