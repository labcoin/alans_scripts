Experimental ID:		HT29_100vMiSeq_3_GC 
Reference Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/MiSeq_Normal/DOWNSAMPLE20_NORMAL.bam 
Tumour Bam:			/mnt/coin/arobertson/PIPELINE3/MAPPED/EHLC_HT29_100/EHLC_HT29_100_FSD.bam 
MODE:				2 
Location:			1_0mb_X_300mb 
Window Size:			100 Kb 
Beta Down Weight:		10 
Ploidy / BackgroundCount:	3 
GC Correction:			TRUE 
##Estimated cellularity:1.0
##Estimated ratio:1.048
##Version:2.0
Max. possible CNVs:		31371.61
Number of CNVs:			78
Total length of CNVs:		1435600004 bp
Average Length of CNV:		18405128.2564103 bp
WARNING:			NONE
