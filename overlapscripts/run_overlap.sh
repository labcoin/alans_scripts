##type=2 means use both deletions and duplications
##type=1 means deletions only
##type = 3 means duplications only

##first command does overall 
#Rscript ../run_overlap_analysis.R type=2 len_thresh=0.02,Inf cumR=1

##second command splits into length bands
#Rscript ../run_overlap_analysis.R type=2 len_thresh=0.05,1,Inf cumR=1

Rscript ../run_overlap_analysis.R type=2 len_thresh=0.1,Inf cumR=1 rankMax=3
